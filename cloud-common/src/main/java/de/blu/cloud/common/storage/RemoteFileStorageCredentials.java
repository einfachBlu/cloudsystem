package de.blu.cloud.common.storage;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import de.blu.cloud.api.logging.Logger;
import de.blu.cloud.common.credentials.FileCredentials;
import de.blu.cloud.common.credentials.RemoteHostCredentials;
import lombok.Getter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Singleton
public class RemoteFileStorageCredentials implements FileCredentials {

    private Map<String, String> defaultConfiguration = new HashMap<String, String>() {{
        this.put("protocol", RemoteServerProtocols.SFTP.name());
        this.put("host", "127.0.0.1");
        this.put("port", "22");
        this.put("user", "root");
        this.put("password", "yourpassword123456");
        this.put("directory", "/cloud/storage/");
    }};

    @Getter
    private Properties properties = new Properties();

    @Getter
    private RemoteHostCredentials credentials;

    @Inject
    private RemoteFileStorageCredentials(RemoteHostCredentials remoteHostCredentials) {
        this.credentials = remoteHostCredentials;
    }

    @Override
    public void load(File file) {
        if (file == null || !file.exists()) {
            this.createIfNotExist(file);
            return;
        }

        try {
            this.getProperties().load(new FileInputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            this.getCredentials().setProtocol(this.getProperties().getProperty("protocol"));
            this.getCredentials().setHost(this.getProperties().getProperty("host"));
            this.getCredentials().setPort(Integer.parseInt(this.getProperties().getProperty("port")));
            this.getCredentials().setUser(this.getProperties().getProperty("user"));
            this.getCredentials().setPassword(this.getProperties().getProperty("password"));
            this.getCredentials().setDirectory(this.getProperties().getProperty("directory"));
        } catch (NumberFormatException e) {
            Logger.getGlobal().error("The Port in the RemoteHost Configuration is not a Number!");
        }
    }

    @Override
    public void save(File file) {
        if (file == null || !file.exists()) {
            this.createIfNotExist(file);
            return;
        }

        this.getProperties().setProperty("protocol", this.getCredentials().getProtocol());

        if (!this.getCredentials().getProtocol().equalsIgnoreCase(RemoteServerProtocols.LOCAL.name())) {
            this.getProperties().setProperty("host", this.getCredentials().getHost());
            this.getProperties().setProperty("port", String.valueOf(this.getCredentials().getPort()));
            this.getProperties().setProperty("user", this.getCredentials().getUser());
            this.getProperties().setProperty("password", this.getCredentials().getPassword());
        }
        this.getProperties().setProperty("directory", this.getCredentials().getDirectory());

        try {
            this.getProperties().store(new FileWriter(file), "" +
                    "Availabel Protocols: " + Arrays.toString(RemoteServerProtocols.values()) + "\n" +
                    "This Credentials will be used to connect to the" + "\n" +
                    "Storage Server which is a must have for this CloudSystem." + "\n" +
                    "On the Storage Server will be the global Configurations." + "\n" +
                    "");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createIfNotExist(File file) {
        // Set Defaults
        for (Map.Entry<String, String> entry : this.defaultConfiguration.entrySet()) {
            this.getProperties().setProperty(entry.getKey(), entry.getValue());
        }

        try {
            this.getProperties().store(new FileWriter(file), "" +
                    "Availabel Protocols: " + Arrays.toString(RemoteServerProtocols.values()) + "\n" +
                    "This Credentials will be used to connect to the" + "\n" +
                    "Storage Server which is a must have for this CloudSystem." + "\n" +
                    "On the Storage Server will be the global Configurations." + "\n" +
                    "");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Check if the File has still the Configuration
     *
     * @return true if everything is like default or false if something was changed
     */
    public boolean isDefaultConfiguration() {
        for (Object key : this.getProperties().keySet()) {
            if (!this.getProperties().containsKey(key)) {
                return false;
            }

            if (!this.getProperties().getProperty(String.valueOf(key)).equalsIgnoreCase(this.defaultConfiguration.getOrDefault(String.valueOf(key), ""))) {
                return false;
            }
        }

        return true;
    }
}
