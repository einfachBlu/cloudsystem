package de.blu.cloud.node.setup;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import de.blu.cloud.api.logging.Logger;
import de.blu.cloud.common.command.ConsoleInputReader;
import de.blu.cloud.common.credentials.RemoteHostCredentials;
import de.blu.cloud.common.storage.RemoteServerProtocols;
import de.blu.cloud.node.CloudNode;
import lombok.Getter;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;

@Singleton
public final class RemoteStorageSetup {

    @Getter
    private ExecutorService threadPool;

    @Getter
    private Logger logger;

    @Getter
    private ConsoleInputReader consoleInputReader;

    @Getter
    private RemoteHostCredentials credentials;

    @Inject
    private RemoteStorageSetup(ExecutorService threadPool, Logger logger, ConsoleInputReader consoleInputReader, RemoteHostCredentials credentials) {
        this.threadPool = threadPool;
        this.logger = logger;
        this.consoleInputReader = consoleInputReader;
        this.credentials = credentials;
    }

    public void startSetup(Consumer<RemoteHostCredentials> credentialsCallback) {
        this.getLogger().info("");
        this.getLogger().info("&bYou need to Setup the Remote-Storage-Server before you can continue!");

        this.askForProtocol();
        this.getLogger().info("&aUsing Protocol '" + this.getCredentials().getProtocol() + "'");
        if (!this.getCredentials().getProtocol().equalsIgnoreCase(RemoteServerProtocols.LOCAL.name())) {
            this.askForHost();
            this.getLogger().info("&aUsing Host '" + this.getCredentials().getHost() + "'");
            this.askForPort();
            this.getLogger().info("&aUsing Port '" + this.getCredentials().getPort() + "'");
            this.askForUser();
            this.getLogger().info("&aUsing User '" + this.getCredentials().getUser() + "'");
            this.askForPassword();
            this.getLogger().info("&aUsing Password '" + this.getCredentials().getPassword() + "'");
        }

        this.askForDirectory();
        this.getLogger().info("&aUsing Directory '" + this.getCredentials().getDirectory() + "'");

        this.getLogger().info("&bYour Connection to the Remote-Storage-Server is now set.");

        credentialsCallback.accept(this.getCredentials());
    }

    private void askForProtocol() {
        String defaultProtocol = RemoteServerProtocols.SFTP.name();
        this.getLogger().info("Which Protocol do you want to use? (default &e" + defaultProtocol + "&r)");
        String protocolName = this.getConsoleInputReader().readLine();

        if (protocolName.equalsIgnoreCase("")) {
            protocolName = defaultProtocol;
        }

        try {
            RemoteServerProtocols protocol = RemoteServerProtocols.valueOf(protocolName.toUpperCase());
            this.getCredentials().setProtocol(protocol.name());
        } catch (IllegalArgumentException e) {
            this.getLogger().error("This is not a valid Protocol!");
            this.getLogger().error("Available Protocols: " + Arrays.toString(Arrays.stream(RemoteServerProtocols.values()).toArray()));
            this.askForProtocol();
        }
    }

    private void askForHost() {
        String defaultHost = "127.0.0.1";
        this.getLogger().info("What is the Hostname/IP of the Storage Server? (default &e" + defaultHost + "&r)");
        String host = this.getConsoleInputReader().readLine();

        if (host.equalsIgnoreCase("")) {
            host = defaultHost;
        }

        // Check if Hostname is reachable
        boolean reachable = false;
        try {
            reachable = InetAddress.getByName(host).isReachable(2500);
            if (!reachable) {
                this.getLogger().error("The Hostname/IP is not reachable!");
            }
        } catch (Exception e) {
            this.getLogger().error("The Hostname/IP is not valid!");
        }

        if (!reachable) {
            this.askForHost();
            return;
        }

        this.getCredentials().setHost(host);
    }

    private void askForPort() {
        int defaultPort = 0;
        int port;

        for (RemoteServerProtocols protocol : RemoteServerProtocols.values()) {
            if (!this.getCredentials().getProtocol().equalsIgnoreCase(protocol.name())) {
                continue;
            }

            defaultPort = protocol.getDefaultPort();
        }

        this.getLogger().info("What is the Port of the Storage Server? (default &e" + defaultPort + "&r)");
        String portString = this.getConsoleInputReader().readLine();

        if (portString.equalsIgnoreCase("")) {
            portString = String.valueOf(defaultPort);
        }

        try {
            port = Integer.parseInt(portString);
        } catch (NumberFormatException e) {
            this.getLogger().error("The Port must be a Number!");
            this.askForPort();
            return;
        }

        if (port <= 0 || port > 65535) {
            this.getLogger().error("The Port must be a Number between 1 and 65535!");
            this.askForPort();
            return;
        }

        // Check if Hostname is reachable
        boolean reachable = false;
        reachable = this.isRemoteHostReachable(this.getCredentials().getHost(), port, 2500);
        if (!reachable) {
            this.getLogger().error("Could not ping " + this.getCredentials().getHost() + ":" + port);
        }

        if (!reachable) {
            this.askForPort();
            return;
        }

        this.getCredentials().setPort(port);
    }

    private void askForUser() {
        String defaultUser = "root";
        this.getLogger().info("Which User should be used for Authentication to your Storage Server? (default &e" + defaultUser + "&r)");

        String user = this.getConsoleInputReader().readLine();
        if (user.equalsIgnoreCase("")) {
            user = defaultUser;
        }

        this.getCredentials().setUser(user);
    }

    private void askForPassword() {
        this.getLogger().info("Which Password is the right for the User " + this.getCredentials().getUser() + " to your Storage Server?");

        String password = this.getConsoleInputReader().readLine();
        this.getCredentials().setPassword(password);
    }

    private void askForDirectory() {
        String defaultDirectory = "/cloud/storage/";
        if (this.getCredentials().getProtocol().equalsIgnoreCase(RemoteServerProtocols.LOCAL.name())) {
            File localTemplateDirectory = new File(CloudNode.getRootDirectory().getParent(), "storage");
            if (!localTemplateDirectory.exists()) {
                localTemplateDirectory.mkdirs();
            }
            
            defaultDirectory = localTemplateDirectory.getAbsolutePath();
        }
        this.getLogger().info("What should be the base Directory for your Storage Server? (default &e" + defaultDirectory + "&r)");

        String directory = this.getConsoleInputReader().readLine();
        if (directory.equalsIgnoreCase("")) {
            directory = defaultDirectory;
        }

        this.getCredentials().setDirectory(directory);
    }

    /**
     * Overriding default InetAddress.isReachable() method to add 2 more arguments port and timeout value
     * <p>
     * Address: www.google.com
     * port: 80 or 443
     * timeout: 2000 (in milliseconds)
     */
    private boolean isRemoteHostReachable(String address, int port, int timeout) {
        try {

            try (Socket crunchifySocket = new Socket()) {
                // Connects this socket to the server with a specified timeout value.
                crunchifySocket.connect(new InetSocketAddress(address, port), timeout);
            }
            // Return true if connection successful
            return true;
        } catch (IOException exception) {
            //exception.printStackTrace();

            // Return false if connection fails
            return false;
        }
    }
}
