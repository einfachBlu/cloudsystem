package de.blu.cloud.common.redis;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.lambdaworks.redis.ClientOptions;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisURI;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.sync.RedisCommands;
import com.lambdaworks.redis.pubsub.RedisPubSubListener;
import com.lambdaworks.redis.pubsub.StatefulRedisPubSubConnection;
import de.blu.cloud.api.database.RedisConnection;
import de.blu.cloud.api.database.redis.RedisListener;
import de.blu.cloud.common.credentials.Credentials;
import lombok.Getter;

import java.util.*;

@Singleton
public class RedisConnectionProvider implements RedisConnection {
    private static final int REDIS_EXPIRE_DEFAULT = 30 * 24 * 60 * 60; // 30days

    @Getter
    public RedisClient client;

    @Getter
    public StatefulRedisConnection<String, String> connectionCache;

    @Getter
    public StatefulRedisPubSubConnection<String, String> connectionPubsubListener;

    @Getter
    public StatefulRedisPubSubConnection<String, String> connectionPubsubPublish;

    @Getter
    public RedisCommands<String, String> redisCommandsCache;

    @Getter
    public RedisCommands<String, String> redisCommandsPubsubListener;

    @Getter
    public RedisCommands<String, String> redisCommandsPubsubPublish;

    @Getter
    private Credentials credentials;

    @Inject
    private RedisConnectionProvider(RedisCredentials credentials) {
        this.credentials = credentials;
    }

    @Override
    public void connect() {
        try {
            if (this.getCredentials().getPassword().equalsIgnoreCase("")) {
                this.client = RedisClient.create(
                        RedisURI.builder()
                                .withHost(this.getCredentials().getHost())
                                .withPort(this.getCredentials().getPort())
                                .build()
                );
            } else {
                this.client = RedisClient.create(
                        RedisURI.builder()
                                .withHost(this.getCredentials().getHost())
                                .withPort(this.getCredentials().getPort())
                                .withPassword(this.getCredentials().getPassword())
                                .build()
                );
            }

            this.client.setOptions(
                    ClientOptions.builder()
                            .autoReconnect(true)
                            .disconnectedBehavior(ClientOptions.DisconnectedBehavior.ACCEPT_COMMANDS)
                            .cancelCommandsOnReconnectFailure(false)
                            .build()
            );

            this.connectionCache = this.getClient().connect();
            this.redisCommandsCache = this.getConnectionCache().sync();
            this.connectionPubsubListener = this.getClient().connectPubSub();
            this.connectionPubsubPublish = this.getClient().connectPubSub();
            this.redisCommandsPubsubListener = this.getConnectionPubsubListener().sync();
            this.redisCommandsPubsubPublish = this.getConnectionPubsubPublish().sync();
        } catch (Exception e) {
            this.client = null;
            this.connectionCache = null;
            this.redisCommandsCache = null;
            this.connectionPubsubListener = null;
            this.connectionPubsubPublish = null;
            this.redisCommandsPubsubListener = null;
            this.redisCommandsPubsubPublish = null;
            //System.out.println("Error while connecting to Redis: " + e.getMessage());
        }
    }

    @Override
    public void disconnect() {
        if (this.getConnectionCache() != null) {
            this.getConnectionCache().close();
        }

        if (this.getClient() != null) {
            this.getClient().shutdown();
        }

        this.connectionCache = null;
        this.client = null;

        this.connectionPubsubListener = null;
        this.connectionPubsubPublish = null;

        this.redisCommandsCache = null;
        this.redisCommandsPubsubListener = null;
        this.redisCommandsPubsubPublish = null;
    }

    @Override
    public boolean isConnected() {
        return this.getClient() != null;
    }

    @Override
    public void set(String key, String value) {
        this.set(key, value, REDIS_EXPIRE_DEFAULT);
    }

    @Override
    public void set(String key, String value, int expireSeconds) {
        if (!this.isConnected()) {
            return;
        }

        this.getRedisCommandsCache().set(key, value);
        this.getRedisCommandsCache().expire(key, expireSeconds);
    }

    @Override
    public void remove(String key) {
        if (!this.isConnected()) {
            return;
        }

        this.getRedisCommandsCache().del(key);
    }

    @Override
    public void removeRecursive(String key) {
        if (!this.isConnected()) {
            return;
        }

        for (String recursiveKey : this.getKeys(key)) {
            this.removeRecursive(key + "." + recursiveKey);
        }

        this.remove(key);
    }

    @Override
    public Collection<String> getKeys(String key) {
        return this.getKeys(key, false);
    }

    @Override
    public Collection<String> getKeys(String key, boolean recursive) {
        Set<String> keys = new HashSet<>();
        if (!this.isConnected()) {
            return keys;
        }

        List<String> cachedKeys;
        if (key.equalsIgnoreCase("")) {
            cachedKeys = this.getRedisCommandsCache().keys("*");
        } else {
            cachedKeys = this.getRedisCommandsCache().keys(key + ".*");
        }

        for (String cachedKey : cachedKeys) {
            if (recursive) {
                keys.add(cachedKey);
                continue;
            }

            cachedKey = cachedKey.substring(key.length() + (!key.equalsIgnoreCase("") ? 1 : 0));
            cachedKey = cachedKey.split("\\.")[0];

            keys.add(cachedKey);
        }

        return keys;
    }

    @Override
    public String get(String key) {
        if (!this.isConnected()) {
            return null;
        }

        final String result = this.getRedisCommandsCache().get(key);
        return result;
    }

    @Override
    public Map<String, String> getAll() {
        if (!this.isConnected()) {
            return null;
        }

        List<String> keys = this.getRedisCommandsCache().keys("*");
        Map<String, String> data = new LinkedHashMap<>();

        keys.forEach(key -> {
            data.put(key, this.get(key));
        });

        return data;
    }

    @Override
    public boolean contains(String... key) {
        if (!this.isConnected()) {
            return false;
        }

        final boolean result = this.getRedisCommandsCache().keys(key[0] + "*").size() > 0;
        return result;
    }

    @Override
    public int getRemainingTimeFromKey(String key) {
        if (!this.isConnected()) {
            return -1;
        }

        if (!this.contains(key)) {
            return -1;
        }
        final long remainingTime = this.getRedisCommandsCache().ttl(key);
        return Math.toIntExact(remainingTime);
    }

    @Override
    public void subscribe(RedisListener listener, String... channels) {
        if (!this.isConnected()) {
            return;
        }

        this.getConnectionPubsubListener().addListener(new RedisPubSubListener<String, String>() {
            @Override
            public void message(String channel, String message) {
                if (!channels[0].equalsIgnoreCase(channel)) {
                    return;
                }

                listener.onMessageReceived(channel, message);
            }

            @Override
            public void message(String pattern, String channel, String message) {
            }

            @Override
            public void subscribed(String channel, long count) {
            }

            @Override
            public void psubscribed(String pattern, long count) {
            }

            @Override
            public void unsubscribed(String channel, long count) {
            }

            @Override
            public void punsubscribed(String pattern, long count) {
            }
        });

        this.getConnectionPubsubListener().sync().subscribe(channels);
    }

    @Override
    public boolean channelExists(String channel) {
        if (!this.isConnected()) {
            return false;
        }

        List<String> channels = this.getRedisCommandsPubsubPublish().pubsubChannels(channel);
        return channels.contains(channel);
    }

    @Override
    public void publish(String channel, String message) {
        if (!this.isConnected()) {
            return;
        }

        if (!this.channelExists(channel)) {
            //new Exception("Redis PubSub Channel " + channel + " doesnt exist!").printStackTrace();
            return;
        }

        this.getRedisCommandsPubsubPublish().publish(channel, message);
    }
}