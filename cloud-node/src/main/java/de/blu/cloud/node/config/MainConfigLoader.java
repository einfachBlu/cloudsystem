package de.blu.cloud.node.config;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import de.blu.cloud.api.logging.Logger;
import lombok.Getter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

@Singleton
public final class MainConfigLoader {

    @Getter
    private Logger logger;

    @Getter
    private File rootDirectory;

    @Getter
    private Properties properties = new Properties();

    @Getter
    private File configFile;

    @Getter
    private String nodeName;

    @Getter
    private String host;

    @Inject
    private MainConfigLoader(Logger logger, @Named("rootdir") File rootDirectory) {
        this.logger = logger;
        this.rootDirectory = rootDirectory;
        this.configFile = new File(this.getRootDirectory(), "config.properties");
    }

    public void loadConfig() {
        try {
            if (!this.getConfigFile().exists()) {
                this.getConfigFile().createNewFile();
            }

            this.getProperties().load(new FileReader(this.getConfigFile()));

            if (!this.getProperties().containsKey("name")) {
                this.setNodeName("Node-1");
            }

            if (!this.getProperties().containsKey("host")) {
                this.setHost("-");
            }

            this.nodeName = this.getProperties().getProperty("name");
            this.host = this.getProperties().getProperty("host");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
        this.getProperties().setProperty("name", this.getNodeName());
        this.save();
    }

    public void setHost(String host) {
        this.host = host;
        this.getProperties().setProperty("host", this.getHost());
        this.save();
    }

    private void save() {
        try {
            this.getProperties().store(new FileWriter(this.getConfigFile()), "MainConfig for the Node");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isHostSet() {
        return !this.getHost().equalsIgnoreCase("-");
    }
}
