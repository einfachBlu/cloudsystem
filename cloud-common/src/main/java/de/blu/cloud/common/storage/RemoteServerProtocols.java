package de.blu.cloud.common.storage;

import lombok.Getter;

public enum RemoteServerProtocols {
    SFTP(22),
    FTP(21),
    LOCAL(0);

    @Getter
    private int defaultPort;

    RemoteServerProtocols(int defaultPort) {
        this.defaultPort = defaultPort;
    }
}
