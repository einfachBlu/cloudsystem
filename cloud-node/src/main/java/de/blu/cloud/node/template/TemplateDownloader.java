package de.blu.cloud.node.template;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import de.blu.cloud.api.data.CloudType;
import de.blu.cloud.api.logging.Logger;
import de.blu.cloud.common.credentials.RemoteHostCredentials;
import de.blu.cloud.common.repository.CloudTypeRepository;
import de.blu.cloud.common.storage.RemoteFileStorageProvider;
import de.blu.cloud.common.system.SystemCommandExecutor;
import de.blu.cloud.node.config.MainConfigLoader;
import de.blu.cloud.node.repository.NodesRepository;
import lombok.AccessLevel;
import lombok.Getter;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

/**
 * Will be downloading the Templates from the RemoteHost
 */
@Singleton
@Getter(AccessLevel.PRIVATE)
public final class TemplateDownloader {

    private RemoteHostCredentials remoteHostCredentials;
    private RemoteFileStorageProvider remoteFileStorageProvider;
    private NodesRepository nodesRepository;
    private MainConfigLoader mainConfigLoader;
    private CloudTypeRepository cloudTypeRepository;
    private File tempDirectory;
    private File rootDirectory;
    private TemplateComparator templateComparator;
    private Logger logger;
    private SystemCommandExecutor systemCommandExecutor;

    private Collection<CloudType> downloadCloudTypes = new ArrayList<>();

    @Inject
    private TemplateDownloader(RemoteFileStorageProvider remoteFileStorageProvider, CloudTypeRepository cloudTypeRepository,
                               @Named("tempdir") File tempDirectory, @Named("rootdir") File rootDirectory, NodesRepository nodesRepository,
                               MainConfigLoader mainConfigLoader, TemplateComparator templateComparator, Logger logger, RemoteHostCredentials remoteHostCredentials,
                               SystemCommandExecutor systemCommandExecutor) {
        this.remoteFileStorageProvider = remoteFileStorageProvider;
        this.cloudTypeRepository = cloudTypeRepository;
        this.tempDirectory = tempDirectory;
        this.nodesRepository = nodesRepository;
        this.mainConfigLoader = mainConfigLoader;
        this.rootDirectory = rootDirectory;
        this.templateComparator = templateComparator;
        this.logger = logger;
        this.remoteHostCredentials = remoteHostCredentials;
        this.systemCommandExecutor = systemCommandExecutor;
    }

    public void downloadTemplates() {
        for (CloudType cloudType : this.getCloudTypeRepository().getCloudTypes()) {
            if (cloudType.isStaticService()) {
                continue;
            }

            File templateDirectory = this.createDirectoryIfNotExist(cloudType);
            if (this.getTemplateComparator().isEqualToRemote(cloudType, templateDirectory)) {
                continue;
            }

            this.getDownloadCloudTypes().add(cloudType);
        }

        for (CloudType cloudType : this.getDownloadCloudTypes()) {
            this.downloadTemplate(cloudType);
        }

        // Clear if someone reloads the templates
        this.getDownloadCloudTypes().clear();
    }

    private void downloadTemplate(CloudType cloudType) {
        long time = System.currentTimeMillis();
        this.getLogger().info("Downloading Template for CloudType " + cloudType.getName() + "...");
        File localDirectory = new File(this.getRootDirectory(), "templates/" + cloudType.getName());

        // Archive the Template on the remote
        String targetArchiveFilePath = this.archiveTemplateAndGetPath(cloudType);

        // Download the Archive to temp Directory
        File localTempFile = new File(this.getTempDirectory(), targetArchiveFilePath.substring("temp/".length()));
        this.getRemoteFileStorageProvider().get().downloadFile(targetArchiveFilePath, localTempFile);

        // Delete old Templates Directory
        try {
            FileUtils.deleteDirectory(localDirectory);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Extract Archive from temp Directory to templates Directory
        this.extractAndDeleteTemplateFile(localTempFile, localDirectory.getParentFile());

        // Delete Archive on remote
        this.deleteRemoteArchiveFile(targetArchiveFilePath);

        this.getLogger().info("Download completed. (Took " + (System.currentTimeMillis() - time) + "ms)");
    }

    private void extractAndDeleteTemplateFile(File compressedTemplateFile, File targetDirectory) {
        // Move File from temp -> templates
        try {
            FileUtils.moveFile(compressedTemplateFile, new File(targetDirectory, compressedTemplateFile.getName()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        compressedTemplateFile = new File(targetDirectory, compressedTemplateFile.getName());

        // Extract tar with the Command
        String command = "tar xfz " + compressedTemplateFile.getAbsolutePath() + " --directory " + targetDirectory.getAbsolutePath();
        this.getSystemCommandExecutor().execute(command);

        // Delete compressed file
        compressedTemplateFile.delete();
    }

    private String archiveTemplateAndGetPath(CloudType cloudType) {
        String targetArchiveFilePath = "temp/template_" + cloudType.getName() + "_" + this.getMainConfigLoader().getNodeName() + "_" + UUID.randomUUID() + ".tar.gz";

        String outputFileName = this.getRemoteHostCredentials().getDirectory() + targetArchiveFilePath;
        String targetPath = (this.getRemoteHostCredentials().getDirectory() + cloudType.getTemplatePath()).replaceAll("//", "/");
        if (targetPath.endsWith("/")) {
            targetPath = targetPath.substring(0, targetPath.length() - 1);
        }

        String targetDirectoryName = targetPath.split("/")[targetPath.split("/").length - 1];
        String targetDirectory = targetPath.substring(0, targetPath.length() - targetDirectoryName.length());
        String command = "tar cvfz " + outputFileName + " -C " + targetDirectory + " " + targetDirectoryName;
        this.getRemoteFileStorageProvider().get().executeCommand(command);

        return targetArchiveFilePath;
    }

    private void deleteRemoteArchiveFile(String targetArchiveFilePath) {
        this.getRemoteFileStorageProvider().get().deleteFile(targetArchiveFilePath);
    }

    private File createDirectoryIfNotExist(CloudType cloudType) {
        // Create Template Directory on this Node where the Template should be cached
        File templateDirectory = new File(this.getRootDirectory(), "templates/" + cloudType.getName());
        if (templateDirectory.exists()) {
            return templateDirectory;
        }

        templateDirectory.mkdirs();
        return templateDirectory;
    }
}
