package de.blu.cloud.common.repository;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import de.blu.cloud.api.data.CloudType;
import de.blu.cloud.api.data.GameServerData;
import de.blu.cloud.api.database.RedisConnection;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

@Getter
@Setter
@Singleton
public final class GameServerRepository {

    @Inject
    private RedisConnection redisConnection;

    @Inject
    private CloudTypeRepository cloudTypeRepository;

    @Inject
    private Injector injector;

    private Collection<GameServerData> servers = new ArrayList<>();
    private Collection<GameServerData> proxies = new ArrayList<>();

    public void init() {
        this.getRedisConnection().subscribe((channel, message) -> {
            UUID uuid = UUID.fromString(message.split("_")[0]);
            String cloudTypeName = message.split("_")[1];

            GameServerData gameServerData = this.getGameServerDataFromRedis(uuid, cloudTypeName);
            if (gameServerData == null) {
                System.out.println("Failure while getting gameServerData from Redis after add!");
                return;
            }

            GameServerData existingGameServerData = this.getGameServers()
                    .stream()
                    .filter(gameServerData1 -> gameServerData1.getUniqueId().equals(gameServerData.getUniqueId()))
                    .findFirst().orElse(null);

            if (existingGameServerData != null) {
                return;
            }

            if (gameServerData.getCloudType().getType().equals(CloudType.Type.PROXY)) {
                this.getProxies().add(gameServerData);
            } else {
                this.getServers().add(gameServerData);
            }

            //System.out.println("Added Server " + gameServerData.getName() + " to local cache");
        }, "gameserver_add");

        this.getRedisConnection().subscribe((channel, message) -> {
            UUID uuid = UUID.fromString(message.split("_")[0]);
            String cloudTypeName = message.split("_")[1];

            GameServerData gameServerData = this.getGameServerDataFromRedis(uuid, cloudTypeName);
            if (gameServerData == null) {
                System.out.println("Failure while getting gameServerData from Redis after add!");
                return;
            }

            GameServerData existingGameServerData = this.getGameServers()
                    .stream()
                    .filter(gameServerData1 -> gameServerData1.getUniqueId().equals(gameServerData.getUniqueId()))
                    .findFirst().orElse(null);

            if (existingGameServerData == null) {
                return;
            }

            if (gameServerData.getCloudType().getType().equals(CloudType.Type.PROXY)) {
                this.getProxies().remove(existingGameServerData);
                this.getProxies().add(gameServerData);
            } else {
                this.getServers().remove(existingGameServerData);
                this.getServers().add(gameServerData);
            }

            //System.out.println("Updated Server " + gameServerData.getName() + " in local cache");
        }, "gameserver_update");

        this.loadFromRedis();
    }

    public Collection<GameServerData> getGameServers() {
        Collection<GameServerData> gameServerData = new ArrayList<>();
        gameServerData.addAll(this.getServers());
        gameServerData.addAll(this.getProxies());

        return gameServerData;
    }

    public void loadFromRedis() {
        for (String cloudTypeName : this.getRedisConnection().getKeys("gameserver", false)) {
            CloudType cloudType = this.getCloudTypeRepository().getCloudTypeByName(cloudTypeName);
            for (String gameServerUuid : this.getRedisConnection().getKeys("gameserver." + cloudType.getName(), false)) {
                UUID uuid = UUID.fromString(gameServerUuid);
                GameServerData gameServerData = this.getGameServerDataFromRedis(uuid, cloudTypeName);

                if (cloudType.getType().equals(CloudType.Type.PROXY)) {
                    this.getProxies().add(gameServerData);
                } else {
                    this.getServers().add(gameServerData);
                }
            }
        }
    }

    public void add(GameServerData gameServerData) {
        this.syncGameServerData(gameServerData);

        this.getRedisConnection().publish("gameserver_add", gameServerData.getUniqueId().toString() + "_" + gameServerData.getCloudType().getName());
    }

    public void update(GameServerData gameServerData) {
        this.syncGameServerData(gameServerData);

        this.getRedisConnection().publish("gameserver_update", gameServerData.getUniqueId().toString() + "_" + gameServerData.getCloudType().getName());
    }

    private void syncGameServerData(GameServerData gameServerData) {
        String redisRootKey = gameServerData.getRedisRootKey();

        this.getRedisConnection().set(redisRootKey + ".cloudType", gameServerData.getCloudType().getName());
        this.getRedisConnection().set(redisRootKey + ".name", gameServerData.getName());
        this.getRedisConnection().set(redisRootKey + ".id", String.valueOf(gameServerData.getId()));
        this.getRedisConnection().set(redisRootKey + ".uuid", gameServerData.getUniqueId().toString());
        this.getRedisConnection().set(redisRootKey + ".host", gameServerData.getHost());
        this.getRedisConnection().set(redisRootKey + ".port", String.valueOf(gameServerData.getPort()));
        this.getRedisConnection().set(redisRootKey + ".node", gameServerData.getNode());
        this.getRedisConnection().set(redisRootKey + ".runningState", gameServerData.getRunningState().name());
        this.getRedisConnection().set(redisRootKey + ".onlinePlayers", String.valueOf(gameServerData.getOnlinePlayers()));
        this.getRedisConnection().set(redisRootKey + ".maxPlayers", String.valueOf(gameServerData.getMaxPlayers()));
    }

    public GameServerData getGameServerDataFromRedis(UUID uuid, String cloudTypeName) {
        GameServerData gameServerData = this.getInjector().getInstance(GameServerData.class);

        CloudType cloudType = this.getCloudTypeRepository().getCloudTypeByName(cloudTypeName);
        if (cloudType == null) {
            return null;
        }

        gameServerData.setCloudType(cloudType);
        gameServerData.setUniqueId(uuid);

        String redisRootKey = gameServerData.getRedisRootKey();
        if (!this.getRedisConnection().contains(redisRootKey)) {
            return null;
        }

        gameServerData.setRunningState(GameServerData.RunningState.valueOf(this.getRedisConnection().get(redisRootKey + ".runningState")));
        gameServerData.setName(this.getRedisConnection().get(redisRootKey + ".name"));
        gameServerData.setId(Integer.parseInt(this.getRedisConnection().get(redisRootKey + ".id")));
        gameServerData.setUniqueId(UUID.fromString(this.getRedisConnection().get(redisRootKey + ".uuid")));
        gameServerData.setHost(this.getRedisConnection().get(redisRootKey + ".host"));
        gameServerData.setNode(this.getRedisConnection().get(redisRootKey + ".node"));
        gameServerData.setPort(Integer.parseInt(this.getRedisConnection().get(redisRootKey + ".port")));
        gameServerData.setOnlinePlayers(Integer.parseInt(this.getRedisConnection().get(redisRootKey + ".onlinePlayers")));
        gameServerData.setMaxPlayers(Integer.parseInt(this.getRedisConnection().get(redisRootKey + ".maxPlayers")));

        return gameServerData;
    }

    public Collection<GameServerData> getGameServersByCloudType(CloudType cloudType) {
        return this.getGameServers()
                .stream()
                .filter(gameServer -> gameServer.getCloudType().getName().equalsIgnoreCase(cloudType.getName()))
                .collect(Collectors.toList());
    }
}
