package de.blu.cloud.api.data;

import de.blu.cloud.api.CloudAPI;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public final class CloudPlayer {
    @Getter(AccessLevel.PRIVATE)
    private CloudAPI cloudAPI;

    private UUID uniqueId;
    private String name;
    private String ipAddress;
    private GameServerData bukkitGameServer;
    private GameServerData proxyGameServer;

    public void sendToServer(String targetServerName) {
        this.getCloudAPI().sendPlayerToServer(this.getUniqueId(), targetServerName);
    }
}
