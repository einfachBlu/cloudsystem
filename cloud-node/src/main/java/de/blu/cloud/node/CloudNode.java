package de.blu.cloud.node;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import de.blu.cloud.api.database.RedisConnection;
import de.blu.cloud.api.logging.Color;
import de.blu.cloud.api.logging.Logger;
import de.blu.cloud.common.command.CommandRegister;
import de.blu.cloud.common.command.ConsoleInputReader;
import de.blu.cloud.common.logging.LoggingInitializer;
import de.blu.cloud.common.redis.RemoteRedisConfig;
import de.blu.cloud.common.repository.ApplicationNameRepository;
import de.blu.cloud.common.repository.CloudTypeRepository;
import de.blu.cloud.common.repository.GameServerRepository;
import de.blu.cloud.common.storage.RemoteFileStorageCredentials;
import de.blu.cloud.common.storage.RemoteFileStorageProvider;
import de.blu.cloud.common.util.LibraryUtils;
import de.blu.cloud.node.config.CloudTypeConfigLoader;
import de.blu.cloud.node.config.MainConfigLoader;
import de.blu.cloud.node.config.NodesConfigLoader;
import de.blu.cloud.node.leadership.NodeLeadershipRepository;
import de.blu.cloud.node.module.ModuleSettings;
import de.blu.cloud.node.repository.NodesRepository;
import de.blu.cloud.node.server.GameServerStarter;
import de.blu.cloud.node.server.master.GameServerCoordinator;
import de.blu.cloud.node.setup.MainHostPortSetup;
import de.blu.cloud.node.setup.RemoteStorageSetup;
import de.blu.cloud.node.template.TemplateDownloader;
import de.blu.cloud.node.template.TemplateInitializer;
import lombok.Getter;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.Objects;

@Singleton
@Getter
public final class CloudNode {

    public static void main(String[] args) {
        try {
            // Init Libraries
            LibraryUtils.createLibraryFolder(CloudNode.getRootDirectory());
            LibraryUtils.loadLibraries();

            // Create Injector
            Injector injector = Guice.createInjector(new ModuleSettings());

            // Calling Injected Constructor of CloudNode and start the Node
            injector.getInstance(CloudNode.class);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                System.out.println("Stopping in 3 Seconds...");
                Thread.sleep(3000);
                System.exit(0);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
    }

    public static File getRootDirectory() {
        File directory = null;

        String debugPath = "/Users/fabiblu/Library/Mobile Documents/com~apple~CloudDocs/Programmieren/IntelliJ/IdeaProjects/cloudsystem/cloud-node/build/debugging/";
        boolean isDebug = java.lang.management.ManagementFactory.
                getRuntimeMXBean().
                getInputArguments().toString().contains("jdwp");

        if (isDebug) {
            directory = new File(debugPath);
        } else {
            try {
                directory = new File(CloudNode.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }

        if (!directory.isDirectory()) {
            if (!directory.mkdir()) {
                throw new NullPointerException("Couldn't create root directory!");
            }
        }

        return directory;
    }

    @Inject
    private LoggingInitializer loggingInitializer;

    @Inject
    private CommandRegister commandRegister;

    @Inject
    private ConsoleInputReader consoleInputReader;

    @Inject
    private RemoteFileStorageCredentials remoteFileStorageCredentials;

    @Inject
    private RemoteStorageSetup remoteStorageSetup;

    @Inject
    private MainHostPortSetup mainHostPortSetup;

    @Inject
    private RemoteFileStorageProvider remoteFileStorageProvider;

    @Inject
    private RemoteRedisConfig remoteRedisConfig;

    @Inject
    private RedisConnection redisConnection;

    @Inject
    private CloudTypeConfigLoader cloudTypeConfigLoader;

    @Inject
    private CloudTypeRepository cloudTypeRepository;

    @Inject
    private MainConfigLoader mainConfigLoader;

    @Inject
    private ApplicationNameRepository applicationNameRepository;

    @Inject
    private TemplateInitializer templateInitializer;

    @Inject
    private TemplateDownloader templateDownloader;

    @Inject
    private GameServerCoordinator gameServerCoordinator;

    @Inject
    private NodesConfigLoader nodesConfigLoader;

    @Inject
    private NodesRepository nodesRepository;

    @Inject
    private GameServerRepository gameServerRepository;

    @Inject
    private GameServerStarter gameServerStarter;

    @Inject
    private NodeLeadershipRepository nodeLeadershipRepository;

    private Logger logger;

    @Inject
    private CloudNode(Injector injector) {
        // Inject global variables
        injector.injectMembers(this);

        // Set System Parameters
        System.setProperty("http.agent", "Chrome");

        // Clear local temp Directory
        File tempDirectory = new File(CloudNode.getRootDirectory(), "temp");
        if (!tempDirectory.exists() || Objects.requireNonNull(tempDirectory.listFiles()).length > 0) {
            // Clear because it is not empty
            try {
                FileUtils.cleanDirectory(tempDirectory);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Init Logger
        this.getLoggingInitializer().init(new File(CloudNode.getRootDirectory(), "logs"));
        this.logger = this.getLoggingInitializer().getLogger();

        // Print Logo for a better Design
        this.printLogo();

        // Initialize ConsoleInputReader
        this.getConsoleInputReader().init();

        // Load StorageCredentials from file
        File remoteStorageFile = new File(CloudNode.getRootDirectory(), "remote-storage.properties");
        this.getRemoteFileStorageCredentials().load(remoteStorageFile);

        // Start Setup for Storage Credentials if newly created
        if (this.getRemoteFileStorageCredentials().isDefaultConfiguration()) {
            this.getRemoteStorageSetup().startSetup(credentials -> {
                // Store Credentials in the Config
                this.getRemoteFileStorageCredentials().save(remoteStorageFile);
            });
        }

        // Test RemoteHost Connection
        if (!this.getRemoteFileStorageProvider().get().testConnection()) {
            this.getLogger().error("Could not connect to Storage Server! Check your Credentials in " + remoteStorageFile.getName());
            return;
        }

        this.getLogger().info("Connected to the Storage Server.");

        // Create Temp Directory if not exist
        if (!this.getRemoteFileStorageProvider().get().remoteFileExist("temp")) {
            this.getRemoteFileStorageProvider().get().mkdirs("temp");
        }

        // Handle Redis Remote Connection
        this.getRemoteRedisConfig().load();

        // Connect to Redis
        this.getRedisConnection().connect();
        if (!this.getRedisConnection().isConnected()) {
            this.getLogger().error("Could not connect to Redis! Check your Credentials on the Remote Server (~/redis.properties)");
            return;
        }

        this.getLogger().info("Connected to Redis.");

        // Load Main Config
        this.getMainConfigLoader().loadConfig();

        // Start Setup for Host if not set already
        if (!this.getMainConfigLoader().isHostSet()) {
            this.getMainHostPortSetup().startSetup();
        }

        // Check if a Node with that name is already connected
        this.getApplicationNameRepository().setApplicationName(this.getMainConfigLoader().getNodeName());

        /*
        // Remove from Redis on Application stop
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            this.getRedisConnection().removeRecursive("node." + this.getMainConfigLoader().getNodeName());
        }));
        */

        // Load Nodes Config
        try {
            this.getNodesConfigLoader().initDefaultConfig();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Init NodesRepository
        this.getNodesRepository().init();

        // Init Leadership for Nodes
        this.getNodeLeadershipRepository().init();

        if (this.getNodeLeadershipRepository().isLeader()) {
            // Clear Remote temp Directory because no other nodes seems to be started
            this.getRemoteFileStorageProvider().get().deleteFile("temp");
            this.getRemoteFileStorageProvider().get().mkdirs("temp");
            this.getRedisConnection().removeRecursive("gameserver");
        }

        // Init CloudTypeRepository
        this.getCloudTypeRepository().init();

        // Load CloudTypes Configuration
        try {
            this.getCloudTypeConfigLoader().initDefaultConfig();
        } catch (Exception alreadyHandledException) {
            return;
        }

        // Init GameServerRepository
        this.getGameServerRepository().init();

        // Register Commands
        this.getCommandRegister().registerRecursive("de.blu.cloud.node.command");

        // Init Templates and generate defaults if not exist
        this.getTemplateInitializer().init();

        // Download Templates if remote is newer
        this.getTemplateDownloader().downloadTemplates();

        this.getGameServerCoordinator().init();
        this.getGameServerCoordinator().startServerIfNeeded();

        /*
        /*
        // Reload if an other node was reloaded
        this.getRedisConnection().subscribe((channel, message) -> {
            if (CloudNode.this.getApplicationNameRepository().getApplicationName().equalsIgnoreCase(message)) {
                return;
            }

            CloudNode.this.getConsoleInputReader().dispatch("reload");
        }, "node_reload");

        // Init ServerCoordinator and start Server if possible
        if (this.getNodesRepository().isMasterNode()) {
        }

        // TODO: ONLY TESTING HERE:
        /*
        CloudType cloudType = this.getCloudTypeRepository().getCloudTypes().stream().filter(cloudType2 -> cloudType2.getName().equalsIgnoreCase("Bungee")).findFirst().orElse(null);
        if (cloudType != null) {
            for (int i = 0; i < 1; i++) {
                if (!this.getGameServerStarter().startGameServer(cloudType)) {
                    this.getLogger().warning("GameServer vom CloudType " + cloudType.getName() + " konnte nicht gestartet werden!");
                }
            }
        } else {
            System.out.println("&cTestFehler: CloudType Bungee existiert nicht!");
        }
         */
    }

    private void checkForDuplicatedNodeName() {
        if (this.getRedisConnection().contains("node." + this.getMainConfigLoader().getNodeName())) {
            String absolutePath = this.getRedisConnection().get("node." + this.getMainConfigLoader().getNodeName() + ".absolutePath");
            String address = this.getRedisConnection().get("node." + this.getMainConfigLoader().getNodeName() + ".host");

            if (absolutePath != null && address != null) {
                if (!this.getIPAddress().equalsIgnoreCase(address) || !absolutePath.equalsIgnoreCase(CloudNode.getRootDirectory().getAbsolutePath())) {
                    this.getLogger().warning("A Node with the Name '" + this.getMainConfigLoader().getNodeName() + "' is already connected! Please choose a different Name:");
                    String line = this.getConsoleInputReader().readLine();
                    String nodeName = line.split(" ")[0];

                    this.getMainConfigLoader().setNodeName(nodeName);
                    this.checkForDuplicatedNodeName();
                    return;
                }
            }
        }
    }

    private String getIPAddress() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        return "-";
    }

    private void printLogo() {
        Color logoColor = Color.BRIGHT;
        Color creditsColor = Color.YELLOW;
        this.getLogger().info("");
        this.getLogger().info(logoColor + "  ____ _                 _ ____            _");
        this.getLogger().info(logoColor + " / ___| | ___  _   _  __| / ___| _   _ ___| |_ ___ _ __ ___");
        this.getLogger().info(logoColor + "| |   | |/ _ \\| | | |/ _` \\___ \\| | | / __| __/ _ \\ '_ ` _ \\");
        this.getLogger().info(logoColor + "| |___| | (_) | |_| | (_| |___) | |_| \\__ \\ ||  __/ | | | | |");
        this.getLogger().info(logoColor + " \\____|_|\\___/ \\__,_|\\__,_|____/ \\__, |___/\\__\\___|_| |_| |_|");
        this.getLogger().info(creditsColor + "Developed by Blu" + Color.RESET + logoColor + "                  |___/");
        this.getLogger().info("");
    }
}