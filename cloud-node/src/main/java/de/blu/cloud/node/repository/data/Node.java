package de.blu.cloud.node.repository.data;

import com.esotericsoftware.kryonet.Connection;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Node {
    private String name = "";
    private String host = "127.0.0.1";
    private int port = 4000;
    private String absolutePath = "";
    private boolean activated = false;
    private boolean connected = false;

    @Override
    public String toString() {
        return this.getName();
    }
}
