package de.blu.cloud.node.leadership;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import de.blu.cloud.api.database.RedisConnection;
import de.blu.cloud.node.repository.NodesRepository;
import de.blu.cloud.node.repository.data.Node;
import lombok.Getter;

@Singleton
@Getter
public final class NodeLeadershipRepository {

    @Inject
    private RedisConnection redisConnection;

    @Inject
    private NodesRepository nodesRepository;

    private Node leaderNode;
    private Node nextLeaderNode;

    public void init() {
        // Load leader & nextLeader from Redis
        if (this.getRedisConnection().contains("leadership.leader")) {
            this.leaderNode = this.getNodesRepository().getNodeByName(this.getRedisConnection().get("leadership.leader"));
            if (!this.getLeaderNode().isConnected() || this.getLeaderNode() == this.getNodesRepository().getLocalNode()) {
                this.setLeaderNode(null);
            }
        }
        if (this.getRedisConnection().contains("leadership.nextleader")) {
            this.nextLeaderNode = this.getNodesRepository().getNodeByName(this.getRedisConnection().get("leadership.nextleader"));
            if (!this.getNextLeaderNode().isConnected() || this.getLeaderNode() == this.getNodesRepository().getLocalNode()) {
                this.setNextLeaderNode(null);
            }
        }

        this.registerChangedListener();

        if (this.getLeaderNode() == null) {
            this.setLeaderNode(this.getNodesRepository().getLocalNode());
            return;
        }

        if (this.getNextLeaderNode() == null) {
            this.setNextLeaderNode(this.getNodesRepository().getLocalNode());
            return;
        }
    }

    public boolean isLeader() {
        return this.getNodesRepository().getLocalNode() == this.getLeaderNode();
    }

    public boolean isNextLeader() {
        return this.getNodesRepository().getLocalNode() == this.getNextLeaderNode();
    }

    private void registerChangedListener() {
        // Listener when a node is connected
        this.getRedisConnection().subscribe((channel, message) -> {
            String nodeName = message;

            Node node = this.getNodesRepository().getNodes().stream().filter(node1 -> node1.getName().equalsIgnoreCase(nodeName)).findFirst().orElse(null);
            if (node == null) {
                return;
            }

            this.onNodeConnected(node);
        }, "NodeConnect");

        // Listener when a node is connected
        this.getRedisConnection().subscribe((channel, message) -> {
            String nodeName = message;

            Node node = this.getNodesRepository().getNodeByName(nodeName);
            if (node == null) {
                return;
            }

            this.onNodeDisconnected(node);
        }, "NodeDisconnect");

        // Listener when a node is connected
        this.getRedisConnection().subscribe((channel, message) -> {
            String nodeName = message;

            if (nodeName.equalsIgnoreCase("")) {
                this.leaderNode = null;
                return;
            }

            Node node = this.getNodesRepository().getNodeByName(nodeName);
            if (node == null) {
                return;
            }

            this.leaderNode = node;
            System.out.println("&e" + node.getName() + "&r is now &eLeaderNode");
        }, "LeaderNodeUpdate");

        // Listener when a node is connected
        this.getRedisConnection().subscribe((channel, message) -> {
            String nodeName = message;

            if (nodeName.equalsIgnoreCase("")) {
                this.leaderNode = null;
                return;
            }

            Node node = this.getNodesRepository().getNodeByName(nodeName);
            if (node == null) {
                return;
            }

            this.nextLeaderNode = node;
        }, "NextLeaderNodeUpdate");
    }

    private void onNodeConnected(Node connectedNode) {
    }

    private void onNodeDisconnected(Node disconnectedNode) {
        if (this.getLeaderNode() == disconnectedNode) {
            // LeaderNode disconnected

            if (!this.isNextLeader()) {
                // Only handle by nextLeader
                return;
            }

            this.setLeaderNode(this.getNodesRepository().getLocalNode());
            this.setRandomNextLeader();
        }

        if (!this.isLeader()) {
            // Only handle by leader
            return;
        }

        if (this.getNextLeaderNode() == disconnectedNode) {
            // set random other node as nextLeader
            this.setRandomNextLeader();
        }
    }

    private void setRandomNextLeader() {
        Node nextLeaderNode = null;
        for (Node node : this.getNodesRepository().getNodes()) {
            if (!node.isConnected()) {
                continue;
            }

            if (this.getLeaderNode() == node) {
                continue;
            }

            nextLeaderNode = node;
            break;
        }

        this.setNextLeaderNode(nextLeaderNode);
    }

    private void setLeaderNode(Node node) {
        this.leaderNode = node;

        if (node != null) {
            this.getRedisConnection().set("leadership.leader", node.getName());
            this.getRedisConnection().publish("LeaderNodeUpdate", node.getName());
        } else {
            this.getRedisConnection().remove("leadership.leader");
            this.getRedisConnection().publish("LeaderNodeUpdate", "");
        }
    }

    private void setNextLeaderNode(Node node) {
        this.nextLeaderNode = node;

        if (node != null) {
            this.getRedisConnection().set("leadership.nextleader", node.getName());
            this.getRedisConnection().publish("NextLeaderNodeUpdate", node.getName());
        } else {
            this.getRedisConnection().remove("leadership.nextleader");
            this.getRedisConnection().publish("NextLeaderNodeUpdate", "");
        }
    }
}
