package de.blu.cloud.common.credentials;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Credentials {
    private String protocol;
    private String host;
    private int port;
    private String user;
    private String password;
}
