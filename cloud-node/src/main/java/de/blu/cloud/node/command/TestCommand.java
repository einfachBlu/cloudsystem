package de.blu.cloud.node.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import de.blu.cloud.api.database.RedisConnection;
import de.blu.cloud.api.logging.Color;
import de.blu.cloud.api.logging.Logger;
import de.blu.cloud.common.command.data.Command;
import de.blu.cloud.common.command.data.CommandExecutor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@Command(name = "Test", aliases = {"t"})
@Getter
@Singleton
public final class TestCommand extends CommandExecutor {

    @Inject
    private Logger logger;

    @Inject
    private RedisConnection redisConnection;

    @Override
    public void execute(String label, String[] args) {
        List<String> subCommands = Arrays.asList("colors", "redis", "ansicolors", "clearredis");
        if (args.length == 0) {
            this.displayAllRedisKeys();
            return;
        }

        switch (args[0].toLowerCase()) {
            case "colors":
                this.displayLoggingColors();
                break;
            case "ansicolors":
                this.displayANSIColorCodes();
                break;
            case "redis":
                this.displayAllRedisKeys();
                break;
            case "clearredis":
                this.clearRedis("");
                break;
            default:
                this.getLogger().warning("Optional Sub Commands: " + Arrays.toString(subCommands.toArray()));
                return;
        }
    }

    private void clearRedis(String key) {
        for (String childKey : this.getRedisConnection().getKeys(key, true)) {
            String fullKey = key.isEmpty() ? childKey : key + "." + childKey;
            this.getRedisConnection().remove(fullKey);

            this.clearRedis(fullKey);
        }
    }

    private int i = 0;

    private void displayAllRedisKeys() {
        this.getLogger().debug("All Redis Keys:");
        i = 0;
        this.displayRedisKeysRecursive("");
    }

    private void displayRedisKeysRecursive(String parentKey) {
        for (String key : this.getRedisConnection().getKeys(parentKey, false)) {
            String fullKey;
            if (parentKey.equalsIgnoreCase("")) {
                fullKey = key;
            } else {
                fullKey = parentKey + "." + key;
            }

            if (this.getRedisConnection().getKeys(fullKey, false).size() == 0 || (this.getRedisConnection().get(fullKey) != null)) {
                String color = i == 0 ? "&7" : "&f";
                i = i == 0 ? 1 : 0;
                this.getLogger().debug("&7- " + color + fullKey + " | value=" + this.getRedisConnection().get(fullKey));
            }

            this.displayRedisKeysRecursive(fullKey);
        }
    }

    private void displayANSIColorCodes() {
        for (int i = 0; i < 16; i++) {
            for (int j = 0; j < 16; j++) {
                String code = "" + (i * 16 + j);

                StringBuilder id = new StringBuilder(code);
                while (id.length() < 4) {
                    id.insert(0, " ");
                }
                System.out.print("\u001b[38;5;" + code + "m " + (id));
            }

            System.out.print("\u001b[0m");
            System.out.println();
        }
    }

    private void displayLoggingColors() {
        this.getLogger().debug("All Logging Colors:");
        for (Color color : Color.values()) {
            this.getLogger().debug(color.toString() + color.name());
        }
    }

    private void askForName() {
        this.getLogger().info("What is your Name?");
        String name = this.getConsoleInputReader().readLine();
        this.getLogger().info("Hello " + name + "! Nice to meet you!");
    }
}
