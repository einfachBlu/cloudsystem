package de.blu.cloud.node.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import de.blu.cloud.api.logging.Logger;
import de.blu.cloud.common.command.data.Command;
import de.blu.cloud.common.command.data.CommandData;
import de.blu.cloud.common.command.data.CommandExecutor;
import de.blu.cloud.common.command.ConsoleCommandHandler;
import lombok.Getter;

import java.util.Arrays;

@Command(name = "Help", aliases = {"?"})
@Singleton
public final class HelpCommand extends CommandExecutor {

    @Inject
    @Getter
    private Logger logger;

    @Inject
    @Getter
    private ConsoleCommandHandler consoleCommandHandler;

    @Override
    public void execute(String label, String[] args) {
        this.getLogger().info("&bCommands:");

        for (CommandData command : this.getConsoleCommandHandler().getCommands()) {
            this.getLogger().info("- &e" + command.getName() + " &r(Aliases: &e" + Arrays.toString(command.getAliases()) + "&r)");
        }
    }
}
