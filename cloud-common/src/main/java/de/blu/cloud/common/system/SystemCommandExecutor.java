package de.blu.cloud.common.system;

import com.google.inject.Singleton;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

@Singleton
public final class SystemCommandExecutor {

    public void execute(String command) {
        try {
            long time = System.currentTimeMillis();
            int timeoutSeconds = 15;
            Process process = Runtime.getRuntime().exec(command);
            //System.out.println("Execute Command: " + command);

            // Need to handle InputStream of Process, otherwise it will stuck sometimes
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                //System.out.println("PROCESS » " + line);
            }

            // Wait until Process is finished or the timeoutSeconds time was reached
            process.waitFor(timeoutSeconds, TimeUnit.SECONDS);
            if (time + (timeoutSeconds * 1000) <= System.currentTimeMillis()) {
                System.out.println("SystemCommandExecutor timed out after " + timeoutSeconds + " seconds! Maybe the timeOut is not high enough?");
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
