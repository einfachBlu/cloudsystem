package de.blu.cloud.common.repository;

import com.google.inject.Singleton;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Singleton
public final class ApplicationNameRepository {
    private String applicationName;
}
