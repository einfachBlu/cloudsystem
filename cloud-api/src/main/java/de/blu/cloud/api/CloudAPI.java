package de.blu.cloud.api;

import de.blu.cloud.api.data.CloudPlayer;
import de.blu.cloud.api.data.CloudType;
import de.blu.cloud.api.data.GameServerData;
import lombok.Getter;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

public abstract class CloudAPI {

    @Getter
    private static CloudAPI instance;

    public CloudAPI() {
        CloudAPI.instance = this;
    }

    /**
     * Get the CloudType from the current Server
     *
     * @return The CloudType includes the Data
     */
    public abstract CloudType getCloudType();

    /**
     * Get the GameServer Data from the current Server
     *
     * @return GameServer
     */
    public abstract GameServerData getGameServer();

    /**
     * Get all CloudTypes
     *
     * @return Collection with CloudTypes
     */
    public abstract Collection<CloudType> getCloudTypes();

    /**
     * Get all CloudTypes
     *
     * @param callback Callback with Collection with CloudTypes
     */
    public abstract void getCloudTypesAsync(Consumer<Collection<CloudType>> callback);

    /**
     * Get all GameServerData by a given CloudType
     *
     * @param cloudType the CloudType
     * @return Collection with GameServerData
     */
    public abstract Collection<GameServerData> getGameServer(CloudType cloudType);

    /**
     * Get all GameServerData by a given CloudType
     *
     * @param cloudType the CloudType
     * @param callback  Callback with Collection with GameServerData
     */
    public abstract void getGameServerAsync(CloudType cloudType, Consumer<Collection<GameServerData>> callback);

    /**
     * Get GameServerData by a ServerName
     *
     * @param name the name of the Server
     * @return GameServerData or null if nothing was found
     */
    public abstract GameServerData getGameServer(String name);

    /**
     * Get GameServerData by a ServerName
     *
     * @param name     the name of the Server
     * @param callback Callback with GameServerData or null if nothing was found
     */
    public abstract void getGameServerAsync(String name, Consumer<GameServerData> callback);

    /**
     * Get GameServerData by a UniqueId
     *
     * @param serverUniqueId the UUID of the GameServer
     * @return GameServerData or null if nothing was found
     */
    public abstract GameServerData getGameServer(UUID serverUniqueId);

    /**
     * Get GameServerData by a UniqueId
     *
     * @param serverUniqueId the UUID of the GameServer
     * @param callback       Callback with GameServerData or null if nothing was found
     */
    public abstract void getGameServerAsync(UUID serverUniqueId, Consumer<GameServerData> callback);

    /**
     * Get all online CloudPlayers
     *
     * @return Map with CloudPlayers and their UUID
     */
    public abstract Map<UUID, CloudPlayer> getCloudPlayers();

    /**
     * Get all online CloudPlayers
     *
     * @param callback Callback with Map with CloudPlayers and their UUID
     */
    public abstract void getCloudPlayersAsync(Consumer<Map<UUID, CloudPlayer>> callback);

    /**
     * Get a specific CloudPlayer by Name
     *
     * @param playerName the Name of the target Player
     * @return The CloudPlayer or null if the player could not be found
     */
    public abstract CloudPlayer getCloudPlayer(String playerName);

    /**
     * Get a specific CloudPlayer by Name
     *
     * @param playerName the Name of the target Player
     * @param callback   Callback with the CloudPlayer or null if the player could not be found
     */
    public abstract void getCloudPlayerAsync(String playerName, Consumer<CloudPlayer> callback);

    /**
     * Get a specific Player by UUID
     *
     * @param playerUniqueId the UUID of the target Player
     * @return The CloudPlayer or null if the player could not be found
     */
    public abstract CloudPlayer getCloudPlayer(UUID playerUniqueId);

    /**
     * Get a specific Player by UUID
     *
     * @param playerUniqueId the UUID of the target Player
     * @param callback       Callback with the CloudPlayer or null if the player could not be found
     */
    public abstract void getCloudPlayerAsync(UUID playerUniqueId, Consumer<CloudPlayer> callback);

    /**
     * Kick the Player from the Network
     *
     * @param player player to kick
     * @param reason the reason will be displayed in the
     *               client screen after disconnect
     */
    public abstract void kickPlayer(UUID player, String reason);

    /**
     * Send a Player on the Proxy to an other Bukkit Server
     *
     * @param player           the Player
     * @param targetServerName the target Server
     */
    public abstract void sendPlayerToServer(UUID player, String targetServerName);

    /**
     * Start a new GameServer by the given CloudType
     *
     * @param cloudType the server will start with this cloudType
     */
    public abstract void startServer(CloudType cloudType);

    /**
     * Stop a GameServer
     *
     * @param gameServerData the server to stop
     */
    public abstract void stopServer(GameServerData gameServerData);
}
