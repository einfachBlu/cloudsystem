package de.blu.cloud.node.config;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import de.blu.cloud.api.data.CloudType;
import de.blu.cloud.api.logging.Logger;
import de.blu.cloud.common.storage.RemoteFileStorageProvider;
import de.blu.cloud.common.util.JsonFormatter;
import de.blu.cloud.node.repository.NodesRepository;
import de.blu.cloud.node.repository.data.Node;
import lombok.Getter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

@Singleton
public final class NodesConfigLoader {

    @Inject
    @Getter
    private RemoteFileStorageProvider remoteFileStorageProvider;

    @Inject
    @Getter
    private Logger logger;

    @Inject
    @Getter
    @Named("tempdir")
    private File tempDirectory;

    @Inject
    @Getter
    private NodesRepository nodesRepository;

    @Inject
    @Getter
    private MainConfigLoader mainConfigLoader;

    public void initDefaultConfig() throws Exception {
        if (this.getRemoteFileStorageProvider().get().remoteFileExist("nodes.json")) {
            this.downloadConfig();
            this.loadConfig();

            this.getLogger().info("Loaded Nodes: " + Arrays.toString(this.getNodesRepository().getNodes().toArray()));
            return;
        }

        this.saveDefaultConfig();
        this.uploadConfig();
        this.loadConfig();

        this.getLogger().info("Nodes Configuration was created on the Storage Server at ~/nodes.json");
        this.getLogger().info("Loaded Nodes: " + Arrays.toString(this.getNodesRepository().getNodes().toArray()));
    }

    public void reload() {
        try {
            this.initDefaultConfig();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveDefaultConfig() throws Exception {
        File targetFile = new File(this.getTempDirectory(), "nodes.json");
        if (!targetFile.exists()) {
            targetFile.createNewFile();
        }

        JSONObject jsonObject = new JSONObject();
        JSONObject nodeObject = new JSONObject();

        nodeObject.put("host", this.getMainConfigLoader().getHost());
        nodeObject.put("port", 4000);
        nodeObject.put("activated", true);
        jsonObject.put(this.getMainConfigLoader().getNodeName(), nodeObject);

        FileWriter fileWriter = new FileWriter(targetFile);
        fileWriter.write(JsonFormatter.format(jsonObject.toJSONString()));
        fileWriter.flush();
        fileWriter.close();
    }

    public void uploadConfig() {
        this.getRemoteFileStorageProvider().get().uploadFile(new File(this.getTempDirectory(), "nodes.json"), "nodes.json");
    }

    public void downloadConfig() {
        this.getRemoteFileStorageProvider().get().downloadFile("nodes.json", new File(this.getTempDirectory(), "nodes.json"));
    }

    public void loadConfig() throws Exception {
        File configFile = new File(this.getTempDirectory(), "nodes.json");
        JSONObject jsonObject = (JSONObject) JSONValue.parse(new FileReader(configFile));

        Collection<Node> nodes = new ArrayList<>();
        for (Object nodeName : jsonObject.keySet()) {
            Node node = new Node();
            node.setName((String) nodeName);

            JSONObject nodeData = (JSONObject) jsonObject.get(nodeName);

            node.setHost((String) nodeData.get("host"));
            node.setPort((int) ((long) nodeData.get("port")));
            node.setActivated((boolean) nodeData.get("activated"));

            nodes.add(node);
        }

        this.getNodesRepository().setNodes(nodes);
        configFile.delete();
    }

    private CloudType loadGeneralCloudType(CloudType cloudType, JSONObject data) {
        JSONArray inheritances = (JSONArray) data.get("inheritances");
        for (Object inheritance : inheritances) {
            cloudType.getInheritances().add(String.valueOf(inheritance));
        }

        cloudType.setTemplatePath(String.valueOf(data.get("templatePath")));

        if (!cloudType.getType().equals(CloudType.Type.TEMPLATE)) {
            boolean staticService = (boolean) data.get("staticService");
            boolean maintenance = (boolean) data.get("maintenance");
            long minOnlineServers = (long) data.get("minOnlineServers");
            long maxOnlineServers = (long) data.get("maxOnlineServers");
            long portStart = (long) data.get("portStart");
            long portEnd = (long) data.get("portEnd");
            long memory = (long) data.get("memory");
            JSONArray nodes = (JSONArray) data.get("nodes");

            cloudType.setStaticService(staticService);
            cloudType.setMaintenance(maintenance);
            cloudType.setMinOnlineServers((int) minOnlineServers);
            cloudType.setMaxOnlineServers((int) maxOnlineServers);
            cloudType.setPortStart((int) portStart);
            cloudType.setPortEnd((int) portEnd);
            cloudType.setMemory((int) memory);

            for (Object nodeName : nodes) {
                cloudType.getNodes().add(String.valueOf(nodeName));
            }
        }

        return cloudType;
    }

    private CloudType loadTemplateCloudType(CloudType cloudType, JSONObject data) {
        cloudType.setType(CloudType.Type.TEMPLATE);
        // Nothing specific
        return cloudType;
    }

    private CloudType loadBukkitCloudType(CloudType cloudType, JSONObject data) {
        // Nothing specific
        return cloudType;
    }

    private CloudType loadProxyCloudType(CloudType cloudType, JSONObject data) {
        cloudType.setType(CloudType.Type.PROXY);

        JSONArray fallbackPriorities = (JSONArray) data.get("fallbackPriorities");

        for (Object fallbackCloudTypeName : fallbackPriorities) {
            cloudType.getProxyFallbackPriorities().add(String.valueOf(fallbackCloudTypeName));
        }

        return cloudType;
    }
}
