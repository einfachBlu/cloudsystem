package de.blu.cloud.api.data;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collection;

@Getter
@Setter
public final class CloudType {

    private String name = "";
    private Type type = Type.BUKKIT;
    private int minOnlineServers = 0;
    private int maxOnlineServers = 0;
    private int portStart = 0;
    private int portEnd = 0;
    private boolean maintenance = false;
    private int memory = 0;
    private String templatePath = "/" + this.name + "/";
    private Collection<String> nodes = new ArrayList<>();
    private Collection<String> inheritances = new ArrayList<>();
    private Collection<String> proxyFallbackPriorities = new ArrayList<>();
    private boolean staticService = false;

    public enum Type {
        BUKKIT, PROXY, TEMPLATE
    }

    @Override
    public boolean equals(Object object) {
        return ((CloudType) object).getName().equalsIgnoreCase(this.getName());
    }

    @Override
    public String toString() {
        return this.getName();
    }

    /*
    /**
     * Get the Inheritances of a CloudType as CloudTypeObject
     *
     * @param allCloudTypes a collection with all existing CloudTypes
     * @return the inherited CloudTypes
     *
    public Collection<CloudType> getCloudTypeInheritanceAsObjects(Collection<CloudType> allCloudTypes) {
        Collection<CloudType> cloudTypes = new ArrayList<>();
        for (String inheritedCloudTypeName : this.getInheritance()) {
            CloudType inheritedCloudType = allCloudTypes.stream().filter(cloudType -> cloudType.getName().equalsIgnoreCase(inheritedCloudTypeName)).findFirst().orElse(null);
            if (inheritedCloudType == null) {
                continue;
            }

            if (cloudTypes.contains(inheritedCloudType)) {
                continue;
            }

            cloudTypes.add(inheritedCloudType);
        }

        return cloudTypes;
    }

    /**
     * Get the Inheritances of a CloudType as CloudTypeObject recursive
     *
     * @param allCloudTypes a collection with all existing CloudTypes
     * @return the inherited CloudTypes
     *
    public Collection<CloudType> getCloudTypeInheritanceAsObjectsRecursive(Collection<CloudType> cloudTypes, Collection<CloudType> allCloudTypes) {
        for (String inheritedCloudTypeName : this.getInheritance()) {
            CloudType inheritedCloudType = allCloudTypes.stream().filter(cloudType -> cloudType.getName().equalsIgnoreCase(inheritedCloudTypeName)).findFirst().orElse(null);
            if (inheritedCloudType == null) {
                System.out.println("CloudType doesnt exist: " + inheritedCloudTypeName);
                continue;
            }

            if (cloudTypes.contains(inheritedCloudType)) {
                continue;
            }

            cloudTypes.add(inheritedCloudType);
            inheritedCloudType.getCloudTypeInheritanceAsObjectsRecursive(cloudTypes, allCloudTypes);
        }

        return cloudTypes;
    }

    public static Collection<CloudType> getCloudTypesFromRedis(RedisConnection redisConnection) {
        Collection<CloudType> cloudTypes = new ArrayList<>();

        for (String cloudTypeName : redisConnection.getKeys("cloudtype")) {
            CloudType cloudType = new CloudType();
            cloudType.setName(cloudTypeName);
            cloudType.setStatic(Boolean.parseBoolean(redisConnection.getFromRedis("cloudtype." + cloudType.getName() + ".static")));
            cloudType.setMaintenance(Boolean.parseBoolean(redisConnection.getFromRedis("cloudtype." + cloudType.getName() + ".maintenance")));
            cloudType.setServerType(ServerType.valueOf(redisConnection.getFromRedis("cloudtype." + cloudType.getName() + ".serverType")));
            cloudType.setMinOnlineServers(Integer.parseInt(redisConnection.getFromRedis("cloudtype." + cloudType.getName() + ".minOnlineServers")));
            cloudType.setMaxOnlineServers(Integer.parseInt(redisConnection.getFromRedis("cloudtype." + cloudType.getName() + ".maxOnlineServers")));
            cloudType.setMemory(Integer.parseInt(redisConnection.getFromRedis("cloudtype." + cloudType.getName() + ".memory")));
            cloudType.setJoinPower(Integer.parseInt(redisConnection.getFromRedis("cloudtype." + cloudType.getName() + ".joinPower")));
            cloudType.setServerPath(redisConnection.getFromRedis("cloudtype." + cloudType.getName() + ".serverPath"));

            // nodes
            cloudType.setNodes(Arrays.asList(redisConnection.getFromRedis("cloudtype." + cloudType.getName() + ".nodes").split(",")));

            // inheritance
            cloudType.setInheritance(Arrays.asList(redisConnection.getFromRedis("cloudtype." + cloudType.getName() + ".inheritance").split(",")));

            // fallbackpriorities
            cloudType.setProxyFallbackPriorities(Arrays.asList(redisConnection.getFromRedis("cloudtype." + cloudType.getName() + ".fallbackPriorities").split(",")));

            cloudTypes.add(cloudType);
        }

        return cloudTypes;
    }
    */
}
