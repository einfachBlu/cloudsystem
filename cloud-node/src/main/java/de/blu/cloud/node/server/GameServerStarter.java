package de.blu.cloud.node.server;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import de.blu.cloud.api.data.GameServerData;
import de.blu.cloud.api.database.RedisConnection;
import de.blu.cloud.api.exception.NoPortAvailableException;
import de.blu.cloud.common.repository.GameServerRepository;
import de.blu.cloud.node.config.MainConfigLoader;
import de.blu.cloud.node.server.gameserver.GameServer;
import de.blu.cloud.node.server.gameserver.ProxyGameServer;
import lombok.AccessLevel;
import lombok.Getter;

import java.io.IOException;
import java.net.Socket;

@Singleton
@Getter(AccessLevel.PRIVATE)
public final class GameServerStarter {

    private Injector injector;

    @Inject
    private GameServerRepository gameServerRepository;

    @Inject
    private RedisConnection redisConnection;

    @Inject
    private MainConfigLoader mainConfigLoader;

    @Inject
    private GameServerStarter(Injector injector) {
        this.injector = injector;
        this.getInjector().injectMembers(this);
    }

    /**
     * Starting a GameServer on this node
     *
     * @param gameServerData data from the Server
     */
    public synchronized boolean startGameServer(GameServerData gameServerData) {
        Class<? extends GameServer> gameServerClass;
        switch (gameServerData.getCloudType().getType()) {
            case PROXY:
                gameServerClass = ProxyGameServer.class;
                break;
                /*
            case BUKKIT:
                gameServerClass = BukkitGameServer.class;
                break;
                */
            default:
                return false;
        }

        if (gameServerClass == null) {
            return false;
        }

        GameServer gameServer = this.getInjector().getInstance(gameServerClass);
        gameServer.setData(gameServerData);

        try {
            gameServer.setPort(this.getAvailablePort(gameServer));
        } catch (NoPortAvailableException e) {
            return false;
        }

        // Sync with other nodes because of node & port setted now
        this.getGameServerRepository().update(gameServer);

        if (!gameServer.initDirectory()) {
            return false;
        }

        if (!gameServer.start()) {
            return false;
        }

        return true;
    }

    public boolean startGameServer(String targetNodeName, GameServerData gameServerData) {
        return false;
    }

    private int getAvailablePort(GameServerData gameServerData) throws NoPortAvailableException {
        for (int port = gameServerData.getCloudType().getPortStart(); port <= gameServerData.getCloudType().getPortEnd(); port++) {
            if (this.isPortInUse(gameServerData.getHost(), port)) {
                //System.out.println("Port " + port + " is in use!");
                continue;
            }

            return port;
        }

        throw new NoPortAvailableException();
    }

    private boolean isPortInUse(String host, int port) {
        // Check in existing online Servers
        for (GameServerData gameServerData : this.getGameServerRepository().getGameServers()) {
            if (!gameServerData.getHost().equalsIgnoreCase(host)) {
                continue;
            }

            if (gameServerData.getPort() != port) {
                continue;
            }

            return true;
        }

        // Check in Network
        Socket socket = null;
        try {
            socket = new Socket(host, port);
            return true;
        } catch (IOException e) {
            return false;
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
