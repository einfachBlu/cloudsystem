package de.blu.cloud.node.server.master;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import de.blu.cloud.api.data.CloudType;
import de.blu.cloud.api.data.GameServerData;
import de.blu.cloud.api.database.RedisConnection;
import de.blu.cloud.api.logging.Logger;
import de.blu.cloud.common.repository.CloudTypeRepository;
import de.blu.cloud.common.repository.GameServerRepository;
import de.blu.cloud.node.leadership.NodeLeadershipRepository;
import de.blu.cloud.node.repository.NodesRepository;
import de.blu.cloud.node.repository.data.Node;
import de.blu.cloud.node.server.GameServerStarter;
import de.blu.cloud.node.server.factory.GameServerFactory;
import lombok.AccessLevel;
import lombok.Getter;

@Singleton
@Getter(AccessLevel.PRIVATE)
public final class GameServerCoordinator {

    @Inject
    private RedisConnection redisConnection;

    @Inject
    private CloudTypeRepository cloudTypeRepository;

    @Inject
    private GameServerRepository gameServerRepository;

    @Inject
    private GameServerStarter gameServerStarter;

    @Inject
    private NodesRepository nodesRepository;

    @Inject
    private Logger logger;

    @Inject
    private GameServerFactory gameServerFactory;

    @Inject
    private NodeLeadershipRepository nodeLeadershipRepository;

    public void init() {
    }

    public synchronized void startServerIfNeeded() {
        // Only allow leader to coordinate Servers
        if (!this.getNodeLeadershipRepository().isLeader()) {
            return;
        }

        cloudTypeLoop:
        for (CloudType cloudType : this.getCloudTypeRepository().getCloudTypes()) {
            // Ignore Template CloudTypes
            if (cloudType.getType().equals(CloudType.Type.TEMPLATE)) {
                continue;
            }

            // Check if every node is online on which the cloudType can start
            for (String nodeName : cloudType.getNodes()) {
                Node node = this.getNodesRepository().getNodeByName(nodeName);
                if (node == null) {
                    continue cloudTypeLoop;
                }

                if (!node.isConnected()) {
                    continue cloudTypeLoop;
                }
            }

            // Check if currentServers < minAmount
            int currentOnlineAmount = this.getGameServerRepository().getGameServersByCloudType(cloudType).size();
            if (currentOnlineAmount >= cloudType.getMinOnlineServers()) {
                continue;
            }

            GameServerData gameServerData = this.getGameServerFactory().createGameServer(cloudType);

            if (gameServerData == null) {
                this.getLogger().warning("Could not start Server of CloudType &e" + cloudType.getName());
                return;
            }

            // Start Server
            Node node = this.getNodesRepository().getNodeByName(gameServerData.getNode());
            if (node == this.getNodesRepository().getLocalNode()) {
                System.out.println("Starting GameServer of CloudType " + cloudType.getName() + " on this Node...");
                boolean success = this.getGameServerStarter().startGameServer(gameServerData);

                if (!success) {
                    this.getLogger().warning("Could not start Server " + gameServerData.getName() + " on this Node");
                }
            } else {
                System.out.println("Starting GameServer of CloudType " + cloudType.getName() + " on " + node.getName() + "...");
                boolean success = this.getGameServerStarter().startGameServer(node.getName(), gameServerData);

                if (!success) {
                    this.getLogger().warning("Could not start Server " + gameServerData.getName() + " on Node " + node.getName());
                }
            }
        }
    }

    public void stopServerIfPossible() {
    }
}