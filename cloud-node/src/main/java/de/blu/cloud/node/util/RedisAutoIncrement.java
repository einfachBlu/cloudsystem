package de.blu.cloud.node.util;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import de.blu.cloud.api.database.RedisConnection;
import lombok.AccessLevel;
import lombok.Getter;

@Singleton
@Getter(AccessLevel.PRIVATE)
public final class RedisAutoIncrement {

    private RedisConnection redisConnection;

    @Inject
    private RedisAutoIncrement(RedisConnection redisConnection) {
        this.redisConnection = redisConnection;
    }

    public void clear() {
        this.getRedisConnection().removeRecursive("redis.ai");
    }

    public int getAndIncrement(String key) {
        this.increment(key);
        return this.get(key) - 1;
    }

    public int incrementAndGet(String key) {
        this.increment(key);
        return this.get(key);
    }

    private void increment(String key) {
        this.set(key, this.get(key) + 1);
    }

    private void set(String key, int value) {
        this.getRedisConnection().set("redis.ai." + key, "" + value);
    }

    public int get(String key) {
        if (this.getRedisConnection().contains(key)) {
            this.set(key, 0);
        }

        return Integer.parseInt(this.getRedisConnection().get("redis.ai." + key));
    }
}
