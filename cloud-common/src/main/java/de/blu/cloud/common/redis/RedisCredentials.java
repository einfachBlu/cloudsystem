package de.blu.cloud.common.redis;

import com.google.inject.Singleton;
import de.blu.cloud.common.credentials.Credentials;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Singleton
public class RedisCredentials extends Credentials {
}
