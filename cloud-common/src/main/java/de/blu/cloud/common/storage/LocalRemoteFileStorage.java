package de.blu.cloud.common.storage;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import de.blu.cloud.api.storage.FileInfo;
import de.blu.cloud.api.storage.RemoteFileStorage;
import de.blu.cloud.common.credentials.RemoteHostCredentials;
import de.blu.cloud.common.system.SystemCommandExecutor;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;

@Singleton
public final class LocalRemoteFileStorage implements RemoteFileStorage {

    private RemoteHostCredentials credentials;
    private SystemCommandExecutor systemCommandExecutor;

    @Inject
    private LocalRemoteFileStorage(RemoteHostCredentials credentials, SystemCommandExecutor systemCommandExecutor) {
        this.credentials = credentials;
        this.systemCommandExecutor = systemCommandExecutor;
    }

    @Override
    public void downloadFile(String remoteFilePath, File localFile) {
        File remoteFile = new File(this.credentials.getDirectory() + remoteFilePath);
        try {
            FileUtils.copyFile(remoteFile, localFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void uploadFile(File localFile, String remoteFilePath) {
        File remoteFile = new File(this.credentials.getDirectory() + remoteFilePath);
        try {
            FileUtils.copyFile(localFile, remoteFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteFile(String remoteFilePath) {
        if (!this.remoteFileExist(remoteFilePath)) {
            return;
        }

        File file = new File(this.credentials.getDirectory() + remoteFilePath);
        file.delete();
    }

    @Override
    public boolean remoteFileExist(String remoteFilePath) {
        return new File(this.credentials.getDirectory() + remoteFilePath).exists();
    }

    @Override
    public void mkdirs(String remoteDirectoryPath) {
        File directory = new File(this.credentials.getDirectory() + remoteDirectoryPath);
        directory.mkdirs();
    }

    @Override
    public Collection<String> listFiles(String remoteDirectoryPath) {
        Collection<String> files = new HashSet<>();

        File directory = new File(this.credentials.getDirectory() + remoteDirectoryPath);
        for (File file : directory.listFiles()) {
            files.add(file.getName());
        }

        return files;
    }

    @Override
    public FileInfo getFileInfo(String remoteFilePath) {
        FileInfo fileInfo = new FileInfo(this.credentials.getDirectory() + remoteFilePath, remoteFilePath);
        File file = new File(fileInfo.getAbsolutePath());

        fileInfo.setExist(file.exists());

        if (fileInfo.exist()) {
            fileInfo.setIsDirectory(file.isDirectory());
            fileInfo.setLastModified(file.lastModified());
            fileInfo.setSize(file.length());
        }

        return fileInfo;
    }

    @Override
    public Collection<FileInfo> getFilesOfDirectory(String remoteDirectoryPath) {
        Collection<FileInfo> files = new HashSet<>();

        File file = new File(this.credentials.getDirectory() + remoteDirectoryPath);
        if (!file.exists()) {
            return files;
        }

        for (File childFile : file.listFiles()) {
            files.add(this.getFileInfo(childFile.getAbsolutePath().substring(this.credentials.getDirectory().length())));
        }

        return files;
    }

    @Override
    public Collection<FileInfo> getFilesOfDirectoryRecursive(String remoteDirectoryPath) {
        Collection<FileInfo> files = new HashSet<>();

        File file = new File(this.credentials.getDirectory() + remoteDirectoryPath);
        if (!file.exists()) {
            return files;
        }

        for (File childFile : file.listFiles()) {
            FileInfo fileInfo = this.getFileInfo(childFile.getAbsolutePath().substring(this.credentials.getDirectory().length()));
            files.add(fileInfo);

            if (fileInfo.isDirectory()) {
                files.addAll(this.getFilesOfDirectoryRecursive(fileInfo.getRelativePath()));
            }
        }

        return files;
    }

    @Override
    public void executeCommand(String command) {
        this.systemCommandExecutor.execute(command);
    }

    @Override
    public boolean testConnection() {
        return true;
    }
}
