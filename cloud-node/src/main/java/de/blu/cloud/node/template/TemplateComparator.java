package de.blu.cloud.node.template;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import de.blu.cloud.api.data.CloudType;
import de.blu.cloud.api.storage.FileInfo;
import de.blu.cloud.common.storage.RemoteFileStorageProvider;
import lombok.AccessLevel;
import lombok.Getter;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;

/**
 * Will compare local & remote Templates and check if all is equal to each other
 */
@Singleton
@Getter(AccessLevel.PRIVATE)
public final class TemplateComparator {

    private RemoteFileStorageProvider remoteFileStorageProvider;

    // The Value of this Variable will be set to false if there was
    // something found which is not equals on local & remote
    private boolean equals = true;

    private CloudType cloudType = null;
    private File localDirectory = null;
    private Collection<FileInfo> remoteFiles = new HashSet<>();

    @Inject
    private TemplateComparator(RemoteFileStorageProvider remoteFileStorageProvider) {
        this.remoteFileStorageProvider = remoteFileStorageProvider;
    }

    public synchronized boolean isEqualToRemote(CloudType cloudType, File localDirectory) {
        this.equals = true;
        this.cloudType = cloudType;
        this.localDirectory = localDirectory;
        this.remoteFiles = this.getRemoteFileStorageProvider().get().getFilesOfDirectoryRecursive(this.getCloudType().getTemplatePath());

        //System.out.println("&bTemplateComparator comparing CloudType " + this.getCloudType().getName() + " with local dir: " + this.getLocalDirectory().getAbsolutePath());

        // Check if any File is not in local Directory
        this.scanForNewFiles();

        // Check if any File from local Directory is not anymore in remote Directory
        this.scanForDeletedFiles();

        // Check if the lastModified Value of the Remote Files are equals to the local files
        this.scanForModifiedFiles();

        this.cloudType = null;
        this.localDirectory = null;
        this.remoteFiles = null;
        return this.isEquals();
    }

    private void scanForNewFiles() {
        for (FileInfo remoteFile : this.getRemoteFiles()) {
            File localFile = new File(this.getLocalDirectory().getParent(), remoteFile.getRelativePath());
            if (localFile.exists()) {
                continue;
            }

            this.equals = false;
            break;
        }
    }

    private void scanForDeletedFiles() {
        for (File localFile : Objects.requireNonNull(this.getLocalDirectory().listFiles())) {
            String remoteFileName = (this.getCloudType().getTemplatePath() + (localFile.getAbsolutePath().substring(this.getLocalDirectory().getAbsolutePath().length()))).replaceAll("//", "/");
            if (remoteFileName.startsWith("/")) {
                remoteFileName = remoteFileName.substring(1);
            }
            String finalRemoteFileName = remoteFileName;
            FileInfo remoteFile = this.getRemoteFiles().stream().filter(fileInfo -> {
                String relativePath = fileInfo.getRelativePath();
                if (fileInfo.getRelativePath().startsWith("/")) {
                    relativePath = relativePath.substring(1);
                }

                return relativePath.equalsIgnoreCase(finalRemoteFileName);
            }).findFirst().orElse(null);
            if (remoteFile != null) {
                continue;
            }

            this.equals = false;
            break;
        }
    }

    private void scanForModifiedFiles() {
        for (FileInfo remoteFile : this.getRemoteFiles()) {
            File localFile = new File(this.getLocalDirectory().getParent(), remoteFile.getRelativePath());
            if (!localFile.exists()) {
                continue;
            }

            long localLastModified = localFile.lastModified();
            long remoteLastModified = remoteFile.getLastModified();

            // Ignore if local is newer than remote file
            if (localLastModified >= remoteLastModified) {
                continue;
            }

            this.equals = false;
            break;
        }
    }
}
