package de.blu.cloud.node.template;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import de.blu.cloud.api.data.CloudType;
import de.blu.cloud.common.repository.CloudTypeRepository;
import de.blu.cloud.common.storage.RemoteFileStorageProvider;
import de.blu.cloud.node.config.MainConfigLoader;
import de.blu.cloud.node.repository.NodesRepository;
import lombok.Getter;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.UUID;

/**
 * Will be generating Template Directories and Files if not exist
 */
@Singleton
public final class TemplateInitializer {

    @Getter
    private RemoteFileStorageProvider remoteFileStorageProvider;

    @Getter
    private NodesRepository nodesRepository;

    @Getter
    private MainConfigLoader mainConfigLoader;

    @Getter
    private CloudTypeRepository cloudTypeRepository;

    @Getter
    private File tempDirectory;

    @Getter
    private File rootDirectory;

    // If there exist a BukkitBase & BungeeBase Directory, the server-software.jar's will be placed there
    @Getter
    private boolean baseSetup = false;

    @Inject
    private TemplateInitializer(RemoteFileStorageProvider remoteFileStorageProvider, CloudTypeRepository cloudTypeRepository, @Named("tempdir") File tempDirectory, @Named("rootdir") File rootDirectory, NodesRepository nodesRepository, MainConfigLoader mainConfigLoader) {
        this.remoteFileStorageProvider = remoteFileStorageProvider;
        this.cloudTypeRepository = cloudTypeRepository;
        this.tempDirectory = tempDirectory;
        this.nodesRepository = nodesRepository;
        this.mainConfigLoader = mainConfigLoader;
        this.rootDirectory = rootDirectory;
    }

    public void init() {
        // Check for BaseSetup
        if (this.getCloudTypeRepository().getCloudTypes().stream().filter(cloudType -> cloudType.getName().equalsIgnoreCase("BukkitBase") || cloudType.getName().equalsIgnoreCase("BungeeBase")).count() == 2) {
            this.baseSetup = true;
        }

        /*
        for (CloudType cloudType : this.getCloudTypeRepository().getCloudTypes()) {
            if ((cloudType.isStaticService() && Objects.equals(cloudType.getNodes().stream().findFirst().orElse(null), this.getMainConfigLoader().getNodeName())) || this.getNodesRepository().isMasterNode()) {
                //System.out.println("Files of RemoteTemplate " + cloudType.getName() + ":");
                //System.out.println(Arrays.toString(this.getRemoteFileStorageProvider().get().getFilesOfDirectoryRecursive(cloudType.getTemplatePath()).toArray()));

                this.createDirectoryIfNotExist(cloudType);
                this.downloadServerSoftwareIfNotExist(cloudType);
            }
        }
         */
    }

    private void createDirectoryIfNotExist(CloudType cloudType) {
        if (cloudType.isStaticService()) {
            // Create local static Directory
            File staticDirectory = new File(this.getRootDirectory(), "static/" + cloudType.getName());
            if (staticDirectory.exists()) {
                return;
            }

            staticDirectory.mkdirs();
            return;
        }

        // Create Remote Template Directory
        if (this.getRemoteFileStorageProvider().get().remoteFileExist(cloudType.getTemplatePath())) {
            return;
        }

        this.getRemoteFileStorageProvider().get().mkdirs(cloudType.getTemplatePath());
    }

    private void downloadServerSoftwareIfNotExist(CloudType cloudType) {
        if (cloudType.isStaticService()) {
            // Download ServerSoftware if its not already there
            File staticDirectory = new File(this.getRootDirectory(), "static/" + cloudType.getName());
            File serverSoftwareFile = new File(staticDirectory, "server-software.jar");
            if (serverSoftwareFile.exists()) {
                return;
            }

            System.out.println("Downloading ServerSoftware for Static CloudType &e" + cloudType.getName());
            this.downloadServerSoftware(cloudType.getType(), serverSoftwareFile);
            return;
        }

        CloudType.Type serverSoftwareType = cloudType.getType();
        if (!this.isBaseSetup()) {
            if (cloudType.getType().equals(CloudType.Type.TEMPLATE)) {
                // Template Types dont need an ServerSoftware
                return;
            }
        } else {
            if (!cloudType.getType().equals(CloudType.Type.TEMPLATE)) {
                return;
            }

            if (cloudType.getName().equalsIgnoreCase("BukkitBase")) {
                serverSoftwareType = CloudType.Type.BUKKIT;
            } else if (cloudType.getName().equalsIgnoreCase("BungeeBase")) {
                serverSoftwareType = CloudType.Type.PROXY;
            } else {
                return;
            }
        }

        if (this.getRemoteFileStorageProvider().get().remoteFileExist(cloudType.getTemplatePath() + "server-software.jar")) {
            return;
        }

        System.out.println("Downloading ServerSoftware for CloudType &e" + cloudType.getName());
        File tempFile = new File(tempDirectory, "serversoftware_" + cloudType.getName() + "_" + UUID.randomUUID() + ".jar");
        this.downloadServerSoftware(serverSoftwareType, tempFile);

        // Upload to Storage Template
        this.getRemoteFileStorageProvider().get().uploadFile(tempFile, cloudType.getTemplatePath() + "/server-software.jar");

        // Remove tempFile
        tempFile.delete();
    }

    private void downloadServerSoftware(CloudType.Type type, File targetFile) {
        String downloadUrl;
        switch (type) {
            case PROXY:
                downloadUrl = "https://ci.md-5.net/job/BungeeCord/1486/artifact/bootstrap/target/BungeeCord.jar";
                break;
            case BUKKIT:
                downloadUrl = "https://yivesmirror.com/files/paper/PaperSpigot-1.8.8-R0.1-SNAPSHOT-latest.jar";
                break;
            default:
                return;
        }

        try {
            // Download Software to targetFile
            FileUtils.copyURLToFile(new URL(downloadUrl), targetFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
