package de.blu.cloud.node.setup;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import de.blu.cloud.api.logging.Logger;
import de.blu.cloud.common.command.ConsoleInputReader;
import de.blu.cloud.node.config.MainConfigLoader;
import de.blu.cloud.node.util.AddressResolver;
import lombok.Getter;

import java.io.IOException;
import java.net.InetAddress;

@Singleton
@Getter
public final class MainHostPortSetup {

    @Inject
    private Logger logger;

    @Inject
    private ConsoleInputReader consoleInputReader;

    @Inject
    private MainConfigLoader mainConfigLoader;

    @Inject
    private AddressResolver addressResolver;

    public void startSetup() {
        this.getLogger().info("");
        this.getLogger().info("&bYou need to Setup the Host.");
        this.getLogger().info("&bThe Host must be the external IP Address!");

        this.askForHost();
        this.getLogger().info("&aUsing Host '" + this.getMainConfigLoader().getHost() + "'");

        this.getLogger().info("&bYour Host is now set.");
    }

    private void askForHost() {
        String defaultHost = this.getAddressResolver().getIPAddress();
        this.getLogger().info("What is the Hostname/IP of this Server? (default &e" + defaultHost + "&r)");
        String host = this.getConsoleInputReader().readLine();

        if (host.equalsIgnoreCase("")) {
            host = defaultHost;
        }

        // Check if Hostname is reachable
        boolean reachable = false;
        try {
            reachable = InetAddress.getByName(host).isReachable(2500);
            if (!reachable) {
                this.getLogger().error("The Hostname/IP is not reachable!");
            }
        } catch (Exception e) {
            this.getLogger().error("The Hostname/IP is not valid!");
        }

        if (!reachable) {
            this.askForHost();
            return;
        }

        this.getMainConfigLoader().setHost(host);
    }
}
