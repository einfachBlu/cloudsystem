package de.blu.cloud.api.storage;

import java.io.File;
import java.util.Collection;

public interface RemoteFileStorage {
    /**
     * Download a file from the RemoteStorage
     *
     * @param remoteFilePath the absolute Path from the file
     * @param localFile      the File where the remote File should be saved into
     */
    void downloadFile(String remoteFilePath, File localFile);

    /**
     * Upload a file to the RemoteStorage
     *
     * @param localFile      the local File to upload
     * @param remoteFilePath the absolute Path where the
     *                       File should be stored
     */
    void uploadFile(File localFile, String remoteFilePath);

    /**
     * Delete a File from the RemoteStorage
     *
     * @param remoteFilePath the absolute Path where the
     *                       File is to delete
     */
    void deleteFile(String remoteFilePath);

    /**
     * Check if a File exist on the RemoteHost
     *
     * @param remoteFilePath the local File to check
     */
    boolean remoteFileExist(String remoteFilePath);

    /**
     * Create Directories if not exist
     *
     * @param remoteDirectoryPath the Directory
     */
    void mkdirs(String remoteDirectoryPath);

    /**
     * Get a Collection with all FileNames which are in the Directory
     *
     * @param remoteDirectoryPath the Directory
     * @return Collection with all filenames
     */
    Collection<String> listFiles(String remoteDirectoryPath);

    /**
     * Get Information about a File on the RemoteFileStorage
     *
     * @param remoteFilePath the remotePath of the file
     * @return FileInfo with all needed information about the File
     */
    FileInfo getFileInfo(String remoteFilePath);

    /**
     * Get FileInfo of all Files in the directory on the RemoteFileStorage
     *
     * @param remoteDirectoryPath the remotePath of the directory
     * @return Collection with information about each file in the directory
     */
    Collection<FileInfo> getFilesOfDirectory(String remoteDirectoryPath);

    /**
     * Get FileInfo of all Files in the directory on the RemoteFileStorage recursive
     *
     * @param remoteDirectoryPath the remotePath of the directory
     * @return Collection with information about each file in the directory recursive
     */
    Collection<FileInfo> getFilesOfDirectoryRecursive(String remoteDirectoryPath);

    /**
     * Execute a Command on the Remote System
     *
     * @param command the commandLine to execute
     */
    void executeCommand(String command);

    /**
     * Test if the Connection is working
     *
     * @return true if could connect and false if something failed
     */
    boolean testConnection();
}
