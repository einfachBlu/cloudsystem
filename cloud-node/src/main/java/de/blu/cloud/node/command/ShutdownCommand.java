package de.blu.cloud.node.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import de.blu.cloud.api.logging.Logger;
import de.blu.cloud.common.command.data.Command;
import de.blu.cloud.common.command.data.CommandExecutor;
import lombok.Getter;

@Command(name = "Shutdown", aliases = {"quit"})
@Singleton
public final class ShutdownCommand extends CommandExecutor {

    @Inject
    @Getter
    private Logger logger;

    @Override
    public void execute(String label, String[] args) {
        this.getLogger().info("CloudSystem will be shutdown now...");
        System.exit(0);
    }
}
