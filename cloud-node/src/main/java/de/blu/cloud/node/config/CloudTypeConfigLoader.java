package de.blu.cloud.node.config;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import de.blu.cloud.api.data.CloudType;
import de.blu.cloud.api.logging.Logger;
import de.blu.cloud.common.repository.CloudTypeRepository;
import de.blu.cloud.common.storage.RemoteFileStorageProvider;
import lombok.Getter;
import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

@Singleton
public final class CloudTypeConfigLoader {

    @Inject
    @Getter
    private RemoteFileStorageProvider remoteFileStorageProvider;

    @Inject
    @Getter
    private Logger logger;

    @Inject
    @Getter
    @Named("tempdir")
    private File tempDirectory;

    @Inject
    @Getter
    private CloudTypeRepository cloudTypeRepository;

    public void initDefaultConfig() throws Exception {
        if (this.getRemoteFileStorageProvider().get().remoteFileExist("cloudtypes.json")) {
            this.downloadConfig();
            this.loadConfig();
            this.getCloudTypeRepository().save();

            this.getLogger().info("Loaded CloudTypes: " + Arrays.toString(this.getCloudTypeRepository().getCloudTypes().toArray()));
            return;
        }

        this.saveDefaultConfig();
        this.uploadConfig();
        this.loadConfig();
        this.getCloudTypeRepository().save();

        this.getLogger().info("CloudTypes Configuration was created on the Storage Server at ~/cloudtypes.json");
        this.getLogger().info("Loaded CloudTypes: " + Arrays.toString(this.getCloudTypeRepository().getCloudTypes().toArray()));
    }

    public void reload() {
        try {
            this.initDefaultConfig();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveDefaultConfig() throws Exception {
        File targetFile = new File(this.getTempDirectory(), "cloudtypes.json");
        try {
            FileUtils.copyInputStreamToFile(this.getClass().getResourceAsStream("/config/cloudtypes.json"), targetFile);
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void uploadConfig() {
        this.getRemoteFileStorageProvider().get().uploadFile(new File(this.getTempDirectory(), "cloudtypes.json"), "cloudtypes.json");
    }

    public void downloadConfig() {
        this.getRemoteFileStorageProvider().get().downloadFile("cloudtypes.json", new File(this.getTempDirectory(), "cloudtypes.json"));
    }

    public void loadConfig() throws Exception {
        File configFile = new File(this.getTempDirectory(), "cloudtypes.json");
        JSONObject jsonObject = (JSONObject) JSONValue.parse(new FileReader(configFile));

        Collection<CloudType> cloudTypes = new ArrayList<>();
        for (Object cloudTypeName : jsonObject.keySet()) {
            CloudType cloudType = new CloudType();
            cloudType.setName((String) cloudTypeName);
            JSONObject cloudTypeData = (JSONObject) jsonObject.get(cloudTypeName);

            String typeString = (String) cloudTypeData.get("type");

            CloudType.Type type = CloudType.Type.valueOf(typeString);
            cloudType.setType(type);
            cloudType = this.loadGeneralCloudType(cloudType, cloudTypeData);
            switch (type) {
                case BUKKIT:
                    cloudType = this.loadBukkitCloudType(cloudType, cloudTypeData);
                    break;
                case PROXY:
                    cloudType = this.loadProxyCloudType(cloudType, cloudTypeData);
                    break;
                case TEMPLATE:
                    cloudType = this.loadTemplateCloudType(cloudType, cloudTypeData);
                    break;
                default:
                    break;
            }

            cloudTypes.add(cloudType);
        }

        this.getCloudTypeRepository().setCloudTypes(cloudTypes);

        configFile.delete();
    }

    private CloudType loadGeneralCloudType(CloudType cloudType, JSONObject data) {
        JSONArray inheritances = (JSONArray) data.get("inheritances");
        for (Object inheritance : inheritances) {
            cloudType.getInheritances().add(String.valueOf(inheritance));
        }

        cloudType.setTemplatePath(String.valueOf(data.get("templatePath")));

        if (!cloudType.getType().equals(CloudType.Type.TEMPLATE)) {
            boolean staticService = (boolean) data.get("staticService");
            boolean maintenance = (boolean) data.get("maintenance");
            long minOnlineServers = (long) data.get("minOnlineServers");
            long maxOnlineServers = (long) data.get("maxOnlineServers");
            long portStart = (long) data.get("portStart");
            long portEnd = (long) data.get("portEnd");
            long memory = (long) data.get("memory");
            JSONArray nodes = (JSONArray) data.get("nodes");

            cloudType.setStaticService(staticService);
            cloudType.setMaintenance(maintenance);
            cloudType.setMinOnlineServers((int) minOnlineServers);
            cloudType.setMaxOnlineServers((int) maxOnlineServers);
            cloudType.setPortStart((int) portStart);
            cloudType.setPortEnd((int) portEnd);
            cloudType.setMemory((int) memory);

            for (Object nodeName : nodes) {
                cloudType.getNodes().add(String.valueOf(nodeName));
            }
        }

        return cloudType;
    }

    private CloudType loadTemplateCloudType(CloudType cloudType, JSONObject data) {
        cloudType.setType(CloudType.Type.TEMPLATE);
        // Nothing specific
        return cloudType;
    }

    private CloudType loadBukkitCloudType(CloudType cloudType, JSONObject data) {
        // Nothing specific
        return cloudType;
    }

    private CloudType loadProxyCloudType(CloudType cloudType, JSONObject data) {
        cloudType.setType(CloudType.Type.PROXY);

        JSONArray fallbackPriorities = (JSONArray) data.get("fallbackPriorities");

        for (Object fallbackCloudTypeName : fallbackPriorities) {
            cloudType.getProxyFallbackPriorities().add(String.valueOf(fallbackCloudTypeName));
        }

        return cloudType;
    }
}
