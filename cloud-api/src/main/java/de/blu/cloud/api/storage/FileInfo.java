package de.blu.cloud.api.storage;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class FileInfo {
    private String fileName;
    private String absolutePath;
    private String relativePath;
    private long lastModified;
    private long size;

    // Workaround for renaming the Method "exist" instead of generated "isExist()"
    @Getter(AccessLevel.PRIVATE)
    private boolean exist = false;

    @Getter(AccessLevel.PRIVATE)
    @Setter(AccessLevel.PRIVATE)
    private boolean _isDirectory = false;

    public FileInfo(String absolutePath, String relativePath) {
        this.absolutePath = absolutePath;
        this.relativePath = relativePath;
        this.fileName = this.getFileNameByPath(relativePath);
    }

    public boolean isDirectory() {
        return _isDirectory;
    }

    public void setIsDirectory(boolean isDirectory) {
        this._isDirectory = isDirectory;
    }

    public boolean exist() {
        return exist;
    }

    private String getFileNameByPath(String path) {
        if (path.endsWith("/")) {
            path = path.substring(0, path.length() - 1);
        }

        return path.split("/")[path.split("/").length - 1];
    }

    @Override
    public String toString() {
        return this.getRelativePath();
    }
}
