package de.blu.cloud.common.credentials;

import com.google.inject.Singleton;
import lombok.Getter;
import lombok.Setter;

@Getter
@Singleton
public class RemoteHostCredentials extends Credentials {
    private String directory;

    public void setDirectory(String directory) {
        if(!directory.endsWith("/")){
            directory += "/";
        }

        this.directory = directory;
    }
}
