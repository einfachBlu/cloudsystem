package de.blu.cloud.common.storage;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import de.blu.cloud.api.storage.RemoteFileStorage;
import de.blu.cloud.common.credentials.RemoteHostCredentials;
import lombok.AccessLevel;
import lombok.Getter;

@Singleton
@Getter(AccessLevel.PRIVATE)
public final class RemoteFileStorageProvider {

    @Inject
    private RemoteHostCredentials credentials;

    @Inject
    private LocalRemoteFileStorage localRemoteFileStorage;

    @Inject
    private SFTPRemoteFileStorage sftpRemoteFileStorage;

    @Inject
    private RemoteFileStorageProvider(Injector injector) {
        injector.injectMembers(this);
    }

    public <T extends RemoteFileStorage> T get() {
        RemoteServerProtocols protocol = RemoteServerProtocols.valueOf(this.getCredentials().getProtocol());
        switch (protocol) {
            case FTP:
            case SFTP:
                return (T) this.getSftpRemoteFileStorage();
            case LOCAL:
                return (T) this.getLocalRemoteFileStorage();
        }

        return null;
    }
}
