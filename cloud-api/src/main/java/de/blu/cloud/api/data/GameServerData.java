package de.blu.cloud.api.data;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class GameServerData {
    protected CloudType cloudType;
    protected UUID uniqueId = UUID.randomUUID();
    protected int id = 0;
    protected int onlinePlayers = 0;
    protected int maxPlayers = 0;
    protected String name;
    protected String host;
    protected int port;
    protected String node;
    protected RunningState runningState = RunningState.OFFLINE;

    public String getRedisRootKey() {
        return "gameserver." + this.getCloudType().getName() + "." + this.getUniqueId();
    }

    @Override
    public String toString() {
        return "GameServerData{" +
                "cloudType=" + cloudType +
                ", uniqueId=" + uniqueId +
                ", id=" + id +
                ", onlinePlayers=" + onlinePlayers +
                ", maxPlayers=" + maxPlayers +
                ", name='" + name + '\'' +
                ", host='" + host + '\'' +
                ", port=" + port +
                ", node='" + node + '\'' +
                ", runningState=" + runningState +
                '}';
    }

    public enum RunningState {
        STARTING, ONLINE, STOPPING, OFFLINE
    }
}
