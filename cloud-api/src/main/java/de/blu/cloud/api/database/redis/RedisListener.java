package de.blu.cloud.api.database.redis;

public interface RedisListener {
    void onMessageReceived(String channel, String message);
}
