package de.blu.cloud.client;

import com.google.inject.Guice;
import com.google.inject.Injector;
import de.blu.cloud.client.module.ModuleSettings;
import de.blu.cloud.common.util.LibraryUtils;

import java.io.File;
import java.net.URISyntaxException;

public class CloudClient {
    public static void main(String[] args) {
        try {
            // Init Libraries
            LibraryUtils.createLibraryFolder(CloudClient.getRootDirectory());
            LibraryUtils.loadLibraries();

            // Create Injector
            Injector injector = Guice.createInjector(new ModuleSettings());

            // Calling Injected Constructor of CloudNode and start the Node
            injector.getInstance(CloudClient.class);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                System.out.println("Stopping in 3 Seconds...");
                Thread.sleep(3000);
                System.exit(0);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
    }

    public static File getRootDirectory() {
        File directory = null;

        try {
            directory = new File(CloudClient.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile();
            if (!directory.isDirectory()) {
                if (!directory.mkdir()) {
                    throw new NullPointerException("Couldn't create root directory!");
                }
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return directory;
    }

    public CloudClient() {
        // Here you can start
    }
}
