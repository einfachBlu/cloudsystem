package de.blu.cloud.node.server.gameserver;

import com.google.common.io.Files;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import de.blu.cloud.api.data.CloudType;
import de.blu.cloud.api.data.GameServerData;
import de.blu.cloud.common.repository.CloudTypeRepository;
import de.blu.cloud.node.CloudNode;
import lombok.Getter;

import java.io.*;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;

@Getter
public class GameServer extends GameServerData {

    @Inject
    private CloudTypeRepository cloudTypeRepository;

    @Inject
    @Named("rootdir")
    private File rootDir;

    @Inject
    private ExecutorService pool;

    private BufferedWriter bufferedWriter;
    private BufferedReader bufferedReader;
    private BufferedReader bufferedErrorReader;

    private File tempDirectory;
    private Process process;

    public boolean initDirectory() {
        // Create Temp Directory
        this.tempDirectory = new File(new File(CloudNode.getRootDirectory(), "temp/servers"), this.getName() + "_" + this.getUniqueId().toString());
        this.getTempDirectory().mkdirs();

        this.copyTemplate(this.getCloudType());

        File pluginsDirectory = new File(this.getTempDirectory(), "plugins");
        if (!pluginsDirectory.exists() || Arrays.stream(pluginsDirectory.listFiles()).noneMatch(file -> !file.isDirectory() && file.getName().equalsIgnoreCase("CloudConnector.jar"))) {
            System.out.println("CloudConnector missing");
            return false;
        }

        return true;
    }

    /**
     * Copy Template and all of his Inheritances recursive
     */
    private void copyTemplate(CloudType cloudType) {
        // Copy Inherited Templates
        for (String inheritedCloudTypeName : cloudType.getInheritances()) {
            CloudType inheritedCloudType = this.getCloudTypeRepository().getCloudTypeByName(inheritedCloudTypeName);

            if (inheritedCloudType == null) {
                continue;
            }

            this.copyTemplate(inheritedCloudType);
        }

        // Copy self Template
        this.copyDirectoryContentRecursive(new File(CloudNode.getRootDirectory(), "templates/" + cloudType.getName()), this.getTempDirectory());
    }

    private void copyDirectoryContentRecursive(File fromDir, File toDir) {
        toDir.mkdirs();

        for (File file : fromDir.listFiles()) {
            if (file.isDirectory()) {
                this.copyDirectoryContentRecursive(file, new File(toDir, file.getName()));
                continue;
            }

            try {
                Files.copy(file, new File(toDir, file.getName()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean start() {
        File logFile = new File(this.getRootDir(), "logs/servers/" + this.getName() + "_" + this.getUniqueId());
        logFile.getParentFile().mkdirs();
        try {
            logFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ProcessBuilder processBuilder = new ProcessBuilder(
                "java -jar" +
                        " -Xmx" + this.getCloudType().getMemory() + "M" +
                        /*
                        " -Dtimocloud-servername=" + server.getName() +
                        " -Dtimocloud-serverid=" + server.getId() +
                        " -Dtimocloud-corehost=" + TimoCloudBase.getInstance().getCoreSocketIP() + ":" + TimoCloudBase.getInstance().getCoreSocketPort() +
                        " -Dtimocloud-randommap=" + randomMap +
                        " -Dtimocloud-mapname=" + mapName +
                        " -Dtimocloud-static=" + server.isStatic() +
                        " -Dtimocloud-templatedirectory=" + templateDirectory.getAbsolutePath() +
                        " -Dtimocloud-temporarydirectory=" + temporaryDirectory.getAbsolutePath() +
                        */
                        this.getTempDirectory().getAbsolutePath() + "/server-software.jar" +
                        " -Dcom.mojang.eula.agree=true" /* +
                        "-p " + this.getPort() */
        );

        try {
            this.process = processBuilder.start();

            Runtime.getRuntime().addShutdownHook(new Thread(this::stop));

            this.getPool().execute(() -> {
                try {
                    GameServer.this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(GameServer.this.getProcess().getOutputStream()));
                    GameServer.this.bufferedReader = new BufferedReader(new InputStreamReader(GameServer.this.getProcess().getInputStream()));
                    GameServer.this.bufferedErrorReader = new BufferedReader(new InputStreamReader(GameServer.this.getProcess().getErrorStream()));

                    this.initBufferedReader();

                    this.getProcess().waitFor();
                    this.getPool().execute(GameServer.this::stop);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private void initBufferedReader() {
        this.getPool().execute(() -> {
            try {
                String line;
                while ((line = GameServer.this.getBufferedReader().readLine()) != null) {
                                /*
                                if (GameServer.this.getCloudNode().getScreenServerNames().contains(GameServer.this.getName())) {
                                    ScreenLinePacket screenLinePacket = new ScreenLinePacket(GameServer.this.getName(), line);
                                    GameServer.this.getCloudNode().getNetwork().getNetworkClient().sendTCPPacket(screenLinePacket);
                                }

                                synchronized (GameServer.this.getLog()) {
                                    GameServer.this.getLog().add(line);
                                }
                                 */
                    System.out.println(line);
                }
            } catch (IOException e) {
                if (!e.getMessage().equalsIgnoreCase("Stream closed")) {
                    e.printStackTrace();
                }
            }
        });
        this.getPool().execute(() -> {
            try {
                String line;
                while ((line = GameServer.this.getBufferedErrorReader().readLine()) != null) {
                                /*
                                if (GameServer.this.getCloudNode().getScreenServerNames().contains(GameServer.this.getName())) {
                                    ScreenLinePacket screenLinePacket = new ScreenLinePacket(GameServer.this.getName(), line);
                                    GameServer.this.getCloudNode().getNetwork().getNetworkClient().sendTCPPacket(screenLinePacket);
                                }

                                synchronized (GameServer.this.getLog()) {
                                    GameServer.this.getLog().add(line);
                                }
                                 */
                    System.out.println(line);
                }
            } catch (IOException e) {
                if (!e.getMessage().equalsIgnoreCase("Stream closed")) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void stop() {
        System.out.println("Stopped Server");
        this.getProcess().destroyForcibly();
    }

    public void setData(GameServerData gameServerData) {
        this.setCloudType(gameServerData.getCloudType());
        this.setHost(gameServerData.getHost());
        this.setPort(gameServerData.getPort());
        this.setName(gameServerData.getName());
        this.setId(gameServerData.getId());
        this.setRunningState(gameServerData.getRunningState());
        this.setOnlinePlayers(gameServerData.getOnlinePlayers());
        this.setMaxPlayers(gameServerData.getMaxPlayers());
        this.setNode(gameServerData.getNode());
        this.setUniqueId(gameServerData.getUniqueId());
    }
}
