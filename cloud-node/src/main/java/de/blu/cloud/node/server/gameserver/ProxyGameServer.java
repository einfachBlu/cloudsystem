package de.blu.cloud.node.server.gameserver;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class ProxyGameServer extends GameServer {

    @Override
    public boolean start() {
        try {
            File configFile = new File(this.getTempDirectory(), "config.yml");
            configFile.createNewFile();
            Yaml yaml = new Yaml();
            Map<String, Object> config = (Map<String, Object>) yaml.load(new FileReader(configFile));
            if (config == null) config = new LinkedHashMap<>();
            //config.put("player_limit", this.getMaxPlayersPerProxy());
            List<Map<String, Object>> listeners = (List) config.get("listeners");
            if (listeners == null) listeners = new ArrayList<>();
            Map<String, Object> map = listeners.size() == 0 ? new LinkedHashMap<>() : listeners.get(0);
            //map.put("motd", proxy.getMotd());
            map.put("force_default_server", false);
            map.put("host", "0.0.0.0:" + this.getPort());
            //map.put("max_players", this.getMaxPlayers());
            //if (!proxy.isStatic()) map.put("query_enabled", false);
            if (listeners.size() == 0) listeners.add(map);
            FileWriter writer = new FileWriter(configFile);
            config.put("listeners", listeners);
            DumperOptions dumperOptions = new DumperOptions();
            dumperOptions.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
            new Yaml(dumperOptions).dump(config, writer);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return super.start();
    }
}
