package de.blu.cloud.node.repository;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import de.blu.cloud.api.database.RedisConnection;
import de.blu.cloud.api.logging.Logger;
import de.blu.cloud.node.config.MainConfigLoader;
import de.blu.cloud.node.heartbeat.NodeHeartBeat;
import de.blu.cloud.node.repository.data.Node;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.Timer;
import java.util.TimerTask;

@Getter
@Setter
@Singleton
public final class NodesRepository {

    private Collection<Node> nodes = new HashSet<>();
    private Node localNode;

    @Inject
    @Named("rootdir")
    private File rootDirectory;

    @Inject
    private MainConfigLoader mainConfigLoader;

    @Inject
    private Logger logger;

    @Inject
    private RedisConnection redisConnection;

    @Inject
    private NodeHeartBeat nodeHeartBeat;

    public void init() {
        this.loadActiveNodes();
        this.registerChangedListener();

        // Set currentNode to this
        this.localNode = this.getNodes().stream().filter(node -> node.getName().equalsIgnoreCase(this.getMainConfigLoader().getNodeName())).findFirst().orElse(null);

        if (this.getLocalNode() == null) {
            this.getLogger().warning("You need to add the NodeData to the storage nodes.json File!");
            System.exit(0);
            return;
        }

        if (!this.getLocalNode().isActivated()) {
            this.getLogger().warning("You need to activate this Node in the storage nodes.json File!");
            System.exit(0);
            return;
        }

        this.getLogger().info("Name of this Node is &e" + this.getLocalNode().getName());

        // Set Node Data and sync to all other
        this.getLocalNode().setConnected(true);
        this.getLocalNode().setAbsolutePath(this.getRootDirectory().getAbsolutePath());
        this.getLocalNode().setAbsolutePath(this.getRootDirectory().getAbsolutePath().endsWith("/") ? this.getRootDirectory().getAbsolutePath() : this.getRootDirectory().getAbsolutePath() + "/");
        this.updateNode(this.getLocalNode(), true);

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                NodesRepository.this.getRedisConnection().publish("NodeConnect", NodesRepository.this.getLocalNode().getName());
            }
        }, 300);

        this.getNodeHeartBeat().startTask();
    }

    private void loadActiveNodes() {
        for (Node node : this.getNodes()) {
            this.loadNodeDataFromRedis(node);
        }
    }

    private void registerChangedListener() {
        // Listener for updated node data
        this.getRedisConnection().subscribe((channel, message) -> {
            String senderNodeName = message.split(";")[0];
            String targetNodeName = message.split(";")[1];

            // Ignore duplicated loading
            if (senderNodeName.equalsIgnoreCase(this.getLocalNode().getName())) {
                return;
            }

            // Load Node Data from Redis
            Node node = this.getNodeByName(targetNodeName);
            if (node == null) {
                return;
            }

            this.loadNodeDataFromRedis(node);
        }, "NodeUpdated");

        // Listener when a node is connected
        this.getRedisConnection().subscribe((channel, message) -> {
            String nodeName = message;

            // Load Node Data from Redis
            Node node = this.getNodeByName(nodeName);
            if (node == null) {
                return;
            }

            node.setConnected(true);
            System.out.println("Node &e" + node.getName() + "&r connected.");
        }, "NodeConnect");

        // Listener when a node is connected
        this.getRedisConnection().subscribe((channel, message) -> {
            String nodeName = message;

            // Load Node Data from Redis
            Node node = this.getNodeByName(nodeName);
            if (node == null) {
                return;
            }

            node.setConnected(false);
            System.out.println("Node &e" + node.getName() + "&r disconnected.");
        }, "NodeDisconnect");
    }

    public void updateNode(Node node, boolean notify) {
        // Redis Cache
        this.getRedisConnection().set("node." + node.getName() + ".host", node.getHost(), NodeHeartBeat.TIME_SECONDS + 1);
        this.getRedisConnection().set("node." + node.getName() + ".port", String.valueOf(node.getPort()), NodeHeartBeat.TIME_SECONDS + 1);
        this.getRedisConnection().set("node." + node.getName() + ".absolutePath", node.getAbsolutePath(), NodeHeartBeat.TIME_SECONDS + 1);

        if (notify) {
            // Redis publish
            this.getRedisConnection().publish("NodeUpdated", this.getLocalNode().getName() + ";" + node.getName());
        }
    }

    private void loadNodeDataFromRedis(Node node) {
        if (!this.getRedisConnection().contains("node." + node.getName())) {
            node.setConnected(false);
            return;
        }

        node.setConnected(true);
        node.setHost(this.getRedisConnection().get("node." + node.getName() + ".host"));
        node.setPort(Integer.parseInt(this.getRedisConnection().get("node." + node.getName() + ".port")));
        node.setAbsolutePath(this.getRedisConnection().get("node." + node.getName() + ".absolutePath"));
    }

    public Node getNodeByName(String nodeName) {
        return this.getNodes().stream().filter(node -> node.getName().equalsIgnoreCase(nodeName)).findFirst().orElse(null);
    }
}
