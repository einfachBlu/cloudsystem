package de.blu.cloud.common.storage;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.jcraft.jsch.*;
import de.blu.cloud.api.logging.Logger;
import de.blu.cloud.api.storage.FileInfo;
import de.blu.cloud.api.storage.RemoteFileStorage;
import de.blu.cloud.common.credentials.RemoteHostCredentials;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Singleton
public final class SFTPRemoteFileStorage implements RemoteFileStorage {

    private RemoteHostCredentials credentials;
    private JSch jSch;
    private Session session;
    private ChannelSftp channelSftp;

    private int sessionDownTry = 0;

    @Inject
    private SFTPRemoteFileStorage(RemoteHostCredentials credentials) {
        this.credentials = credentials;
    }

    @Override
    public void downloadFile(String remoteFilePath, File localFile) {
        this.validateRemotePath(remoteFilePath);
        // Create localFile if not exist
        if (!localFile.exists()) {
            localFile.getParentFile().mkdirs();

            try {
                localFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        this.connect();

        try {
            this.channelSftp.get(this.credentials.getDirectory() + remoteFilePath, localFile.getAbsolutePath());
        } catch (SftpException e) {
            e.printStackTrace();
        }

        this.disconnect();
    }

    @Override
    public void uploadFile(File localFile, String remoteFilePath) {
        this.validateRemotePath(remoteFilePath);
        if (!localFile.exists()) {
            Logger.getGlobal().error("Failed to upload a file which not exist! Path: " + localFile.getAbsolutePath());
            return;
        }

        this.connect();

        try {
            this.channelSftp.put(localFile.getAbsolutePath(), this.credentials.getDirectory() + remoteFilePath);
        } catch (SftpException e) {
            if (e.getMessage().equalsIgnoreCase("No such file")) {
                try {
                    this.channelSftp.mkdir(this.credentials.getDirectory());
                    this.uploadFile(localFile, remoteFilePath);
                } catch (SftpException ex) {
                    ex.printStackTrace();
                }
            } else {
                e.printStackTrace();
            }
        } finally {
            this.disconnect();
        }
    }

    @Override
    public void deleteFile(String remoteFilePath) {
        this.validateRemotePath(remoteFilePath);
        this.connect();

        try {
            FileInfo fileInfo = this.getFileInfo(remoteFilePath);
            if (!fileInfo.exist()) {
                return;
            }

            if (fileInfo.isDirectory()) {
                this.deleteDirectoryRecursive(this.credentials.getDirectory() + remoteFilePath);
            } else {
                this.channelSftp.rm(this.credentials.getDirectory() + remoteFilePath);
            }
        } catch (SftpException e) {
            e.printStackTrace();
        } finally {
            this.disconnect();
        }
    }

    private void deleteDirectoryRecursive(String path) throws SftpException {
        // List source directory structure.
        Vector fileAndFolderList = this.channelSftp.ls(path);

        // Iterate objects in the list to get file/folder names.
        Iterator<ChannelSftp.LsEntry> iterator = fileAndFolderList.iterator();
        while (iterator.hasNext()) {
            ChannelSftp.LsEntry item = iterator.next();
            if (!item.getAttrs().isDir()) {
                this.channelSftp.rm(path + "/" + item.getFilename()); // Remove file.
            } else if (!(".".equals(item.getFilename()) || "..".equals(item.getFilename()))) { // If it is a subdir.
                try {
                    // removing sub directory.
                    this.channelSftp.rmdir(path + "/" + item.getFilename());
                } catch (Exception e) { // If subdir is not empty and error occurs.
                    // Do lsFolderRemove on this subdir to enter it and clear its contents.
                    this.deleteDirectoryRecursive(path + "/" + item.getFilename());
                }
            }
        }
        this.channelSftp.rmdir(path); // delete the parent directory after empty
    }

    @Override
    public boolean remoteFileExist(String remoteFilePath) {
        this.validateRemotePath(remoteFilePath);
        this.connect();

        try {
            this.channelSftp.get(this.credentials.getDirectory() + remoteFilePath);
            return true;
        } catch (SftpException e) {
            if (e.getMessage().equalsIgnoreCase("No such file")) {
            } else {
                e.printStackTrace();
            }
        } finally {
            this.disconnect();
        }

        return false;
    }

    private void connect() {
        if (this.isConnected()) {
            return;
        }

        try {
            this.jSch = new JSch();
            this.session = this.jSch.getSession(this.credentials.getUser(), this.credentials.getHost(), this.credentials.getPort());

            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");

            //this.jSch.addIdentity(System.getProperty("user.home") + "/.ssh/id_rsa");

            this.session.setTimeout(60000);
            this.session.setConfig(config);
            this.session.setPassword(this.credentials.getPassword());
            this.session.setServerAliveInterval(1000);
            this.session.connect(30000);

            Channel channel = this.session.openChannel("sftp");
            channel.connect(30000);

            this.channelSftp = (ChannelSftp) channel;
            this.sessionDownTry = 0;
        } catch (JSchException e) {
            if (e.getMessage().equalsIgnoreCase("session is down")) {
                if (this.sessionDownTry > 10) {
                    System.out.println("Jsch Session is down! After 10 Attemps need to stop application!");
                    System.exit(0);
                    return;
                }

                System.out.println("JschSession is down, try again...");
                this.sessionDownTry++;
                this.connect();
            } else {
                e.printStackTrace();
            }
        }
    }

    private void disconnect() {
        if (!this.isConnected()) {
            return;
        }

        this.session.disconnect();
        this.channelSftp.exit();

        this.jSch = null;
        this.session = null;
        this.channelSftp = null;
    }

    private boolean isConnected() {
        return this.jSch != null;
    }

    @Override
    public void mkdirs(String remoteDirectoryPath) {
        this.validateRemotePath(remoteDirectoryPath);
        this.connect();

        try {
            FileInfo fileInfo = this.getFileInfo(remoteDirectoryPath);
            if (fileInfo.exist()) {
                return;
            }

            this.channelSftp.mkdir(this.credentials.getDirectory() + remoteDirectoryPath);
        } catch (SftpException e) {
            if (e.getMessage().equalsIgnoreCase("No such file")) {
            } else {
                e.printStackTrace();
            }
        } finally {
            this.disconnect();
        }
    }

    @Override
    public Collection<String> listFiles(String remoteDirectoryPath) {
        this.validateRemotePath(remoteDirectoryPath);
        Collection<String> files = new HashSet<>();
        this.connect();

        try {
            Vector filelist = this.channelSftp.ls(this.credentials.getDirectory() + remoteDirectoryPath);
            for (int i = 0; i < filelist.size(); i++) {
                ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry) filelist.get(i);

                if (entry.getFilename().equalsIgnoreCase(".") || entry.getFilename().equalsIgnoreCase("..")) {
                    continue;
                }

                files.add(entry.getFilename());
            }
        } catch (SftpException e) {
            if (!e.getMessage().equalsIgnoreCase("No such file")) {
                e.printStackTrace();
            }
        } finally {
            this.disconnect();
        }

        File directory = new File(this.credentials.getDirectory() + remoteDirectoryPath);
        for (File file : directory.listFiles()) {
            files.add(file.getName());
        }

        return files;
    }

    @Override
    public FileInfo getFileInfo(String remoteFilePath) {
        this.validateRemotePath(remoteFilePath);
        FileInfo fileInfo = new FileInfo(this.credentials.getDirectory() + remoteFilePath, remoteFilePath);
        //File file = new File(fileInfo.getAbsolutePath());

        boolean disconnectOnFinish = false;
        if (!this.isConnected()) {
            this.connect();
            disconnectOnFinish = true;
        }

        try {
            Vector filelist = this.channelSftp.ls(fileInfo.getAbsolutePath().substring(0, fileInfo.getAbsolutePath().length() - fileInfo.getFileName().length()));
            for (int i = 0; i < filelist.size(); i++) {
                ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry) filelist.get(i);

                if (!entry.getFilename().equalsIgnoreCase(fileInfo.getFileName())) {
                    continue;
                }

                fileInfo.setExist(true);
                fileInfo.setIsDirectory(entry.getAttrs().isDir());
                fileInfo.setLastModified(((long) entry.getAttrs().getMTime()) * 1000);
                fileInfo.setSize(entry.getAttrs().getSize());
                break;
            }
        } catch (SftpException e) {
            if (!e.getMessage().equalsIgnoreCase("No such file")) {
                e.printStackTrace();
            }
        } finally {
            if (disconnectOnFinish) {
                this.disconnect();
            }
        }

        return fileInfo;
    }

    @Override
    public Collection<FileInfo> getFilesOfDirectory(String remoteDirectoryPath) {
        this.validateRemotePath(remoteDirectoryPath);
        if (!remoteDirectoryPath.endsWith("/")) {
            remoteDirectoryPath = remoteDirectoryPath + "/";
        }
        Collection<FileInfo> files = new HashSet<>();

        this.connect();
        try {
            Vector filelist = this.channelSftp.ls(this.credentials.getDirectory() + remoteDirectoryPath);
            for (int i = 0; i < filelist.size(); i++) {
                ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry) filelist.get(i);

                if (entry.getFilename().equalsIgnoreCase(".") || entry.getFilename().equalsIgnoreCase("..")) {
                    continue;
                }

                FileInfo fileInfo = new FileInfo(this.credentials.getDirectory() + remoteDirectoryPath + entry.getFilename(), remoteDirectoryPath + entry.getFilename());

                fileInfo.setExist(true);
                fileInfo.setIsDirectory(entry.getAttrs().isDir());
                fileInfo.setLastModified(((long) entry.getAttrs().getMTime()) * 1000);
                fileInfo.setSize(entry.getAttrs().getSize());
                files.add(fileInfo);
            }
        } catch (SftpException e) {
            if (!e.getMessage().equalsIgnoreCase("No such file")) {
                e.printStackTrace();
            }
        } finally {
            this.disconnect();
        }

        return files;
    }

    @Override
    public Collection<FileInfo> getFilesOfDirectoryRecursive(String remoteDirectoryPath) {
        this.validateRemotePath(remoteDirectoryPath);
        if (!remoteDirectoryPath.endsWith("/")) {
            remoteDirectoryPath = remoteDirectoryPath + "/";
        }
        Collection<FileInfo> files = new HashSet<>();

        boolean disconnectOnFinish = false;
        if (!this.isConnected()) {
            this.connect();
            disconnectOnFinish = true;
        }

        try {
            Vector filelist = this.channelSftp.ls(this.credentials.getDirectory() + remoteDirectoryPath);
            for (int i = 0; i < filelist.size(); i++) {
                ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry) filelist.get(i);

                if (entry.getFilename().equalsIgnoreCase(".") || entry.getFilename().equalsIgnoreCase("..")) {
                    continue;
                }

                FileInfo fileInfo = new FileInfo(this.credentials.getDirectory() + remoteDirectoryPath + entry.getFilename(), remoteDirectoryPath + entry.getFilename());

                fileInfo.setExist(true);
                fileInfo.setIsDirectory(entry.getAttrs().isDir());
                fileInfo.setLastModified(((long) entry.getAttrs().getMTime()) * 1000);
                fileInfo.setSize(entry.getAttrs().getSize());
                files.add(fileInfo);

                if (fileInfo.isDirectory()) {
                    files.addAll(this.getFilesOfDirectoryRecursive(remoteDirectoryPath + fileInfo.getFileName()));
                }
            }
        } catch (SftpException e) {
            if (!e.getMessage().equalsIgnoreCase("No such file")) {
                e.printStackTrace();
            }
        } finally {
            if (disconnectOnFinish) {
                this.disconnect();
            }
        }

        return files;
    }

    @Override
    public void executeCommand(String command) {
        this.connect();

        StringBuilder outputBuffer = new StringBuilder();
        ChannelExec channel = null;
        try {
            channel = (ChannelExec) this.session.openChannel("exec");
            channel.setCommand(command);
            channel.connect(5000);
            InputStream commandOutput = channel.getInputStream();

            /*
            InputStreamReader inputStreamReader = new InputStreamReader(commandOutput);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println("DEBUGLINE: " + line);
            }
            */

            //int readByte = commandOutput.read();

            /*
            int readByte;
            do {
                readByte = commandOutput.read();
                outputBuffer.append((char) readByte);
            } while (readByte != 0xffffffff);
            */

            channel.disconnect();
        } catch (JSchException | IOException e) {
            if (!e.getMessage().equalsIgnoreCase("No such file")) {
                e.printStackTrace();
            }
        } finally {
            if (channel != null) {
                channel.disconnect();
            }
            this.disconnect();
        }
    }

    private String validateRemotePath(String remotePath) {
        if (remotePath.startsWith("/")) {
            remotePath = remotePath.substring(1);
        }

        if (remotePath.endsWith("/")) {
            remotePath = remotePath.substring(0, remotePath.length() - 1);
        }

        return remotePath;
    }

    @Override
    public boolean testConnection() {
        if (this.isConnected()) {
            return true;
        }

        this.connect();
        if (this.isConnected()) {
            this.disconnect();
            return true;
        }

        return false;
    }
}
