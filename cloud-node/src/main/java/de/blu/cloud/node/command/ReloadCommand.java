package de.blu.cloud.node.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import de.blu.cloud.api.database.RedisConnection;
import de.blu.cloud.api.logging.Logger;
import de.blu.cloud.common.command.data.Command;
import de.blu.cloud.common.command.data.CommandExecutor;
import de.blu.cloud.common.repository.ApplicationNameRepository;
import de.blu.cloud.node.config.CloudTypeConfigLoader;
import lombok.Getter;

@Command(name = "Reload", aliases = {"rl"})
@Singleton
public final class ReloadCommand extends CommandExecutor {

    @Inject
    @Getter
    private CloudTypeConfigLoader cloudTypeConfigLoader;

    @Inject
    @Getter
    private Logger logger;

    @Inject
    @Getter
    private RedisConnection redisConnection;

    @Inject
    @Getter
    private ApplicationNameRepository applicationNameRepository;

    @Override
    public void execute(String label, String[] args) {
        this.getCloudTypeConfigLoader().reload();
        this.getRedisConnection().publish("node_reload", this.getApplicationNameRepository().getApplicationName());
        this.getLogger().info("Configurations reloaded");
    }
}
