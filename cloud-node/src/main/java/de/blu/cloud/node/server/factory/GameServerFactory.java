package de.blu.cloud.node.server.factory;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import de.blu.cloud.api.data.CloudType;
import de.blu.cloud.api.data.GameServerData;
import de.blu.cloud.common.repository.GameServerRepository;
import de.blu.cloud.node.repository.NodesRepository;
import de.blu.cloud.node.repository.data.Node;
import de.blu.cloud.node.server.gameserver.GameServer;
import lombok.Getter;

import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;

@Singleton
@Getter
public final class GameServerFactory {

    @Inject
    private Injector injector;

    @Inject
    private GameServerRepository gameServerRepository;

    @Inject
    private NodesRepository nodesRepository;

    public GameServerData createGameServer(CloudType cloudType) {
        GameServerData gameServerData = this.getInjector().getInstance(GameServer.class);
        gameServerData.setCloudType(cloudType);
        gameServerData.setRunningState(GameServerData.RunningState.STARTING);
        gameServerData.setId(this.getAvailableId(cloudType));
        gameServerData.setName(cloudType.isStaticService() ? cloudType.getName() : cloudType.getName() + "-" + gameServerData.getId());

        // Check for best Node
        Node bestNode = null;
        for (Node node : this.getNodesRepository().getNodes()) {
            if (!cloudType.getNodes().contains(node.getName())) {
                continue;
            }

            if (bestNode == null) {
                bestNode = node;
                continue;
            }

            int nodeRam = this.getRamOfNode(node);
            int bestNodeRam = this.getRamOfNode(bestNode);

            if (nodeRam < bestNodeRam) {
                bestNode = node;
            }

        }

        if (bestNode == null) {
            return null;
        }

        gameServerData.setNode(bestNode.getName());
        gameServerData.setHost(bestNode.getHost());

        // Add to Repository and return the data
        this.getGameServerRepository().add(gameServerData);
        return gameServerData;
    }

    private int getRamOfNode(Node node) {
        int ram = 0;

        for (GameServerData gameServerData : this.getGameServerRepository().getGameServers().stream().filter(gameServer -> gameServer.getNode().equalsIgnoreCase(node.getName())).collect(Collectors.toList())) {
            ram += gameServerData.getCloudType().getMemory();
        }

        return ram;
    }

    private int getAvailableId(CloudType cloudType) {
        Collection<Integer> idsInUse = new HashSet<>();

        for (GameServerData gameServerData : this.getGameServerRepository().getGameServersByCloudType(cloudType)) {
            idsInUse.add(gameServerData.getId());
        }

        for (int i = 1; i < 10000; i++) {
            if (idsInUse.contains(i)) {
                continue;
            }

            return i;
        }

        return 0;
    }
}
