package de.blu.cloud.node.heartbeat;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import de.blu.cloud.api.database.RedisConnection;
import de.blu.cloud.node.leadership.NodeLeadershipRepository;
import de.blu.cloud.node.repository.NodesRepository;
import de.blu.cloud.node.repository.data.Node;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;

@Singleton
@Getter
public final class NodeHeartBeat {

    public static final int TIME_SECONDS = 4;

    private TimerTask task;

    @Inject
    private NodesRepository nodesRepository;

    @Inject
    private RedisConnection redisConnection;

    @Inject
    private NodeLeadershipRepository nodeLeadershipRepository;

    public void startTask() {
        new Timer().schedule(this.task = new TimerTask() {
            @Override
            public void run() {
                // Refresh keys in Redis
                NodeHeartBeat.this.getNodesRepository().updateNode(NodeHeartBeat.this.getNodesRepository().getLocalNode(), false);

                // Only check if nextLeader or leader
                if (!NodeHeartBeat.this.getNodeLeadershipRepository().isLeader() && !NodeHeartBeat.this.getNodeLeadershipRepository().isNextLeader()) {
                    return;
                }

                // Check for expired Nodes
                Collection<Node> expiredNodes = NodeHeartBeat.this.getExpiredNodes();
                if (expiredNodes.size() == 0) {
                    return;
                }

                for (Node expiredNode : expiredNodes) {
                    NodeHeartBeat.this.getRedisConnection().publish("NodeDisconnect", expiredNode.getName());
                    expiredNode.setConnected(false);
                }
            }
        }, TIME_SECONDS * 1000, TIME_SECONDS * 1000);
    }

    public Collection<Node> getExpiredNodes() {
        Collection<Node> expiredNodes = new ArrayList<>();
        for (Node node : this.getNodesRepository().getNodes()) {
            if (!node.isConnected()) {
                continue;
            }

            if (this.getRedisConnection().contains("node." + node.getName())) {
                continue;
            }

            expiredNodes.add(node);
        }

        return expiredNodes;
    }
}
