package de.blu.cloud.common.credentials;

import java.io.File;

public interface FileCredentials {
    /**
     * Load the Credentials from a File
     *
     * @param file the File which contains the Credentials
     */
    void load(File file);

    /**
     * Create default Credentials and save to a File
     *
     * @param file the file where the Credentials should be saved
     */
    void createIfNotExist(File file);

    /**
     * save Credentials to a File
     *
     * @param file the file where the Credentials should be saved
     */
    void save(File file);
}
