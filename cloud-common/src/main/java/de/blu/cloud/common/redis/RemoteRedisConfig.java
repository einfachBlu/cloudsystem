package de.blu.cloud.common.redis;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import de.blu.cloud.api.logging.Logger;
import de.blu.cloud.common.storage.RemoteFileStorageProvider;
import lombok.Getter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

@Singleton
public class RemoteRedisConfig {

    @Getter
    private Properties properties = new Properties() {{
        this.setProperty("host", "127.0.0.1");
        this.setProperty("port", "6379");
        this.setProperty("password", "");
    }};

    @Getter
    private RemoteFileStorageProvider remoteFileStorageProvider;

    @Getter
    private File tempDirectory;

    @Getter
    private RedisCredentials credentials;

    @Inject
    private RemoteRedisConfig(RemoteFileStorageProvider remoteFileStorageProvider, RedisCredentials credentials, @Named("tempdir") File tempDirectory) {
        this.remoteFileStorageProvider = remoteFileStorageProvider;
        this.tempDirectory = tempDirectory;
        this.credentials = credentials;
    }

    public void load() {
        // Check if Config exist on RemoteHost
        if (!this.getRemoteFileStorageProvider().get().remoteFileExist("redis.properties")) {
            this.createIfNotExist();
        }

        // Download Configuration File
        File tempFile = new File(this.getTempDirectory(), "redis.properties");
        this.getRemoteFileStorageProvider().get().downloadFile("redis.properties", tempFile);

        // Read Properties of it
        try {
            this.getProperties().load(new FileInputStream(tempFile));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Delete temp downloaded File
        tempFile.delete();

        try {
            this.getCredentials().setHost(this.getProperties().getProperty("host"));
            this.getCredentials().setPort(Integer.parseInt(this.getProperties().getProperty("port")));
            this.getCredentials().setPassword(this.getProperties().getProperty("password"));
        } catch (Exception e) {
            Logger.getGlobal().error("Error while loading Redis Configuration!");
            Logger.getGlobal().error("Check your redis.properties File on the Storage Server!");
            return;
        }

        System.out.println("Loaded &rRedis Configuration.");
    }

    public void createIfNotExist() {
        System.out.println("Redis Configuration was created on the Storage Server at ~/redis.properties");

        // Create default Config
        File tempFile = new File(this.getTempDirectory(), "redis.properties");
        try {
            this.getProperties().store(new FileWriter(tempFile), "Redis Credentials");
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Upload to Storage Server
        this.getRemoteFileStorageProvider().get().uploadFile(tempFile, "redis.properties");
    }
}
